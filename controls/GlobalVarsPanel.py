from __future__ import absolute_import
from functools import reduce
import wx



# -------------------------------------------------------------------------------
#                              GlobalVars Panel
# -------------------------------------------------------------------------------
from controls.CustomGlobalVarsTable import CustomGlobalVarsTable
from plcopen.VariableInfoCollector import _VariableInfos


class GlobalVarsPanel(wx.Panel):
    """
        Class that implements a panel displaying a tree containing an hierarchical list
        of functions and function blocks available in project an a search control for
        quickly find one functions or function blocks in this list and a text control
        displaying informations about selected functions or function blocks
        """

    def __init__(self, parent, enable_drag=False):
        """
        Constructor
        @param parent: Parent wx.Window of LibraryPanel
        @param enable_drag: Flag indicating that function or function block can
        be drag'n drop from LibraryPanel (default: False)
        """
        wx.Panel.__init__(self, parent, style=wx.TAB_TRAVERSAL)

        # Define LibraryPanel main sizer
        main_sizer = wx.FlexGridSizer(cols=1, hgap=0, rows=2, vgap=0)
        main_sizer.AddGrowableCol(0)
        main_sizer.AddGrowableRow(1)

        # Add SearchCtrl to main sizer
        self.SearchCtrl = wx.SearchCtrl(self)
        # Add a button with a magnifying glass, essentially to show that this
        # control is for searching in tree
        self.SearchCtrl.ShowSearchButton(True)
        self.Bind(wx.EVT_TEXT, self.OnSearchCtrlChanged, self.SearchCtrl)
        self.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN,
                  self.OnSearchButtonClick, self.SearchCtrl)
        # Bind keyboard event on SearchCtrl text control to catch UP and DOWN
        # for search previous and next occurrence

        # This protects from fail to start when no children[0] available (possible for wxPython 3.0)
        if self.SearchCtrl.GetChildren():
            search_textctrl = self.SearchCtrl.GetChildren()[0]
            search_textctrl.Bind(wx.EVT_CHAR, self.OnKeyDown)

        main_sizer.AddWindow(self.SearchCtrl, flag=wx.GROW)

        # Add Splitter window for tree and block comment to main sizer
        splitter_window = wx.SplitterWindow(self)
        splitter_window.SetSashGravity(1.0)
        main_sizer.AddWindow(splitter_window, flag=wx.GROW)


        # Add CustomGrid
        # self.Bind(wx.EVT_TREE_BEGIN_DRAG, self.OnTreeBeginDrag, self.Tree)
        self.myGrid = CustomGlobalVarsTable(splitter_window)

        # Add TextCtrl for function and function block informations
        self.Comment = wx.TextCtrl(splitter_window, size=wx.Size(0, 80),
                                   style=wx.TE_READONLY | wx.TE_MULTILINE)

        splitter_window.SplitHorizontally(self.myGrid.myGrid, self.Comment, -80)

        self.SetSizer(main_sizer)

        # Reference to the project controller
        self.Controller = None

        # Variable storing functions and function blocks library to display
        self.BlockList = None

        self.Values = None
        self.myTextDropTarget = MyTextDropTarget(self)
        # self.myGrid.myGrid.SetDropTarget(MyTextDropTarget(self))
        self.myGrid.myGrid.SetDropTarget(self.myTextDropTarget)
        """
        Destructor
        """
        # Remove reference to project controller
        self.Controller = None

    def SetController(self, controller):
        """
        Set reference to project controller
        @param controller: Reference to project controller
        """
        self.Controller = controller
        self.myTextDropTarget.SetController(controller, self)
    def OnDropText(self, x, y, data):
        temp = data
    def OnVariablesGridCellLeftClick(self, event):
        row = event.GetRow()
        #row1 = self.myGrid.myGrid.GetGridCursorRow()
        # if not self.Debug and (event.GetCol() == 0 and self.Table.GetValueByName(row, "Edit")):
        var_name = self.myGrid.myGrid.GetCellValue(row, 0)
        var_class = "External" #self.Table.GetValueByName(row, "Class")
        var_type = self.myGrid.myGrid.GetCellValue(row, 1)
        #data = wx.TextDataObject(str((var_name, var_class, var_type, self.TagName)))
        data = wx.TextDataObject(str((var_name, var_class, var_type, "G::GLOBAL")))
        dragSource = wx.DropSource(self.myGrid.myGrid)
        dragSource.SetData(data)
        dragSource.DoDragDrop()
        event.Skip()

    def SetBlockList(self, blocklist):
        """
        Set function and function block library to display in TreeCtrl
        @param blocklist: Function and function block library
        """
        # Save functions and function blocks library
        self.BlockList = blocklist
        # Refresh TreeCtrl values
        self.Refresh()

    def SetFocus(self):
        """
        Called to give focus to LibraryPanel
        Override wx.Window SetFocus method
        """
        # Give focus to SearchCtrl
        self.SearchCtrl.SetFocus()

    def Reset(self):
        """
        Reset LibraryPanel values displayed in controls
        """
        # Clear SearchCtrl, TreeCtrl and TextCtrl
        self.SearchCtrl.SetValue("")
        #self.Tree.DeleteAllItems()
        self.Comment.SetValue("")
        self.myGrid.myGrid.ClearGrid()
        if self.myGrid.myGrid.NumberRows > 0:
            self.myGrid.myGrid.DeleteRows(0, self.myGrid.myGrid.NumberRows)

    def Refresh(self):
        """
        Refresh LibraryPanel values displayed in controls
        """
        self.Reset()
        # Get function and function blocks library
        blocktypes = self.BlockList
        if blocktypes is None and self.Controller is not None:
            # Get library from project controller if not defined
            blocktypes = self.Controller.GetBlockTypes()

        if self.Controller == None:
            return

        self.Values = self.Controller.GetConfigurationGlobalVars("config")
        for val in self.Values:
            self.myGrid.myGrid.AppendRows()
            self.myGrid.myGrid.SetCellValue(self.myGrid.myGrid.NumberRows-1, 0, val.Name)
            self.myGrid.myGrid.SetCellValue(self.myGrid.myGrid.NumberRows - 1, 1, val.Type)

    def GetSelectedBlock(self):
        """
        Get selected block informations
        @return: {"type": block_type_name, "inputs": [input_type,...]} or None
        if no block selected
        """
        # Get selected item associated data in tree
        '''
        selected_item = self.Tree.GetSelection()
        selected_pydata = (self.Tree.GetPyData(selected_item)
                           if (selected_item.IsOk() and
                               selected_item != self.Tree.GetRootItem())
                           else None)

        # Return value is None if selected tree item is root or a category
        return ({"type": self.Tree.GetItemText(selected_item),
                 "inputs": selected_pydata["inputs"]}
                if (selected_pydata is not None and
                    selected_pydata["type"] == BLOCK)
                else None)
        '''
    def SelectTreeItem(self, name, inputs):
        """
        Select Tree item corresponding to block informations given
        @param name: Block type name
        @param inputs: List of block inputs type [input_type,...]
        """
        # Find tree item corresponding to block informations
        '''
        item = self.FindTreeItem(self.Tree.GetRootItem(), name, inputs)
        if item is not None and item.IsOk():
            # Select tree item found
            self.Tree.SelectItem(item)
            self.Tree.EnsureVisible(item)
        '''
    def FindTreeItem(self, item, name, inputs=None):
        """
        Find Tree item corresponding to block informations given
        Function is recursive
        @param item: Item to test
        @param name: Block type name
        @param inputs: List of block inputs type [input_type,...]
        """
        # Return immediately if item isn't valid
        if not item.IsOk():
            return None
        '''
        # Get data associated to item to test
        item_pydata = self.Tree.GetPyData(item)
        if item_pydata is not None and item_pydata["type"] == BLOCK:
            # Only test item corresponding to block

            # Test if block inputs type are the same than those given
            type_inputs = item_pydata.get("inputs", None)
            type_extension = item_pydata.get("extension", None)
            if inputs is not None and type_inputs is not None:
                same_inputs = reduce(
                    lambda x, y: x and y,
                    map(
                        lambda x: x[0] == x[1] or x[0] == 'ANY' or x[1] == 'ANY',
                        zip(type_inputs,
                            (inputs[:type_extension]
                             if type_extension is not None
                             else inputs))),
                    True)
            else:
                same_inputs = True

            # Return item if  block data corresponds to informations given
            if self.Tree.GetItemText(item) == name and same_inputs:
                return item

        # Test item children if item doesn't correspond
        child, child_cookie = self.Tree.GetFirstChild(item)
        while child.IsOk():
            result = self.FindTreeItem(child, name, inputs)
            if result:
                return result
            child, child_cookie = self.Tree.GetNextChild(item, child_cookie)
        '''
        return None

    def SearchInTree(self, value, mode="first"):
        """
        Search in Tree and select item that name contains string given
        @param value: String contained in block name to find
        @param mode: Search mode ('first', 'previous' or 'next')
        (default: 'first')
        @return: True if an item was found
        """
        '''
        # Return immediately if root isn't valid
        root = self.Tree.GetRootItem()
        if not root.IsOk():
            return False

        # Set function to navigate in Tree item sibling according to search
        # mode defined
        sibling_function = (self.Tree.GetPrevSibling
                            if mode == "previous"
                            else self.Tree.GetNextSibling)

        # Get current selected item (for next and previous mode)
        item = self.Tree.GetSelection()
        if not item.IsOk() or mode == "first":
            item, _item_cookie = self.Tree.GetFirstChild(root)
            selected = None
        else:
            selected = item

        # Navigate through tree items until one matching found or reach tree
        # starting or ending
        while item.IsOk():

            # Get item data to get item type
            item_pydata = self.Tree.GetPyData(item)

            # Item is a block category
            if (item == root) or item_pydata["type"] == CATEGORY:

                # Get category first or last child according to search mode
                # defined
                child = (self.Tree.GetLastChild(item)
                         if mode == "previous"
                         else self.Tree.GetFirstChild(item)[0])

                # If category has no child, go to sibling category
                item = (child if child.IsOk() else sibling_function(item))

            # Item is a block
            else:

                # Extract item block name
                name = self.Tree.GetItemText(item)
                # Test if block name contains string given
                if name.upper().find(value.upper()) != -1 and item != selected:
                    # Select block and collapse all categories other than block
                    # category
                    child, child_cookie = self.Tree.GetFirstChild(root)
                    while child.IsOk():
                        self.Tree.CollapseAllChildren(child)
                        child, child_cookie = self.Tree.GetNextChild(root, child_cookie)
                    self.Tree.SelectItem(item)
                    self.Tree.EnsureVisible(item)
                    return True

                # Go to next item sibling if block not found
                next = sibling_function(item)

                # If category has no other child, go to next category sibling
                item = (next
                        if next.IsOk()
                        else sibling_function(self.Tree.GetItemParent(item)))
        '''
        return False

    def OnSearchCtrlChanged(self, event):
        """
        Called when SearchCtrl text control value changed
        @param event: TextCtrl change event
        """
        '''
        # Search for block containing SearchCtrl value in 'first' mode
        self.SearchInTree(self.SearchCtrl.GetValue())
        event.Skip()
        '''

    def OnSearchButtonClick(self, event):
        """
        Called when SearchCtrl search button was clicked
        @param event: Button clicked event
        """
        # Search for block containing SearchCtrl value in 'next' mode
        self.SearchInTree(self.SearchCtrl.GetValue(), "next")
        event.Skip()

    def OnTreeItemSelected(self, event):
        """
        Called when tree item is selected
        @param event: wx.TreeEvent
        """
        """
        # Update TextCtrl value with block selected usage
        item_pydata = self.Tree.GetPyData(event.GetItem())
        self.Comment.SetValue(
            item_pydata["comment"]
            if item_pydata is not None and item_pydata["type"] == BLOCK
            else "")

        # Call extra function defined when tree item is selected
        if getattr(self, "_OnTreeItemSelected", None) is not None:
            self._OnTreeItemSelected(event)
        """
        event.Skip()

    def OnTreeBeginDrag(self, event):
        """
        Called when a drag is started in tree
        @param event: wx.TreeEvent
        """

        selected_item = event.GetItem()
        item_pydata = self.Tree.GetPyData(selected_item)
        """
        # Item dragged is a block
        if item_pydata is not None and item_pydata["type"] == BLOCK:
            # Start a drag'n drop
            data = wx.TextDataObject(str(
                (self.Tree.GetItemText(selected_item),
                 item_pydata["block_type"],
                 "",
                 item_pydata["inputs"])))
            dragSource = wx.DropSource(self.Tree)
            dragSource.SetData(data)
            dragSource.DoDragDrop()
        """
    def OnKeyDown(self, event):
        """
        Called when key is pressed in SearchCtrl text control
        @param event: wx.KeyEvent
        """
        # Get event keycode and value in SearchCtrl
        keycode = event.GetKeyCode()
        search_value = self.SearchCtrl.GetValue()

        # Up key was pressed and SearchCtrl isn't empty, search for block in
        # 'previous' mode
        if keycode == wx.WXK_UP and search_value != "":
            self.SearchInTree(search_value, "previous")

        # Down key was pressed and SearchCtrl isn't empty, search for block in
        # 'next' mode
        elif keycode == wx.WXK_DOWN and search_value != "":
            self.SearchInTree(search_value, "next")

        # Handle key normally
        else:
            event.Skip()

    def OnRangeChanged(self, event):
        event.Skip()

class MyTextDropTarget(wx.TextDropTarget):

    def __init__(self, object):

        wx.TextDropTarget.__init__(self)
        self.object = object
        self.Controller = None
        self.Table = None
    def SetController(self, controller, table):
        """
        Set reference to project controller
        @param controller: Reference to project controller
        """
        self.Controller = controller
        self.Table = table

    def OnDropText(self, x, y, data):
        # u'(\'GETBIT_RESULT\', \'External\', \'BOOL\', \'P::CONNECTION\')'
        add = True
        values = eval(data)
        if values[1] == 'External':
            if self.Controller != None:
                globalVars = self.Controller.GetConfigurationGlobalVars("config")
                for val in globalVars:
                    if val.Name == values[0]:
                        add = False
                if add:
                    globalVar = _VariableInfos()
                    globalVar.Class = "Global"
                    globalVar.Documentation = ""
                    globalVar.Edit = True
                    globalVar.InitialValue = ""
                    globalVar.Location = ""
                    globalVar.Name = values[0]
                    # addPouVar.Number
                    globalVar.Option = ""
                    globalVar.Type = values[2]

                    globalVars.append(globalVar)
                    self.Controller.SetConfigurationGlobalVars("config", globalVars)
                    self.Table.Refresh()



