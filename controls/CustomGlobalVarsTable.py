
from __future__ import absolute_import
import wx
import wx.grid

class CustomGlobalVarsTable(wx.grid.Grid):  #

    def __init__(self, parent):

        self.myGrid = wx.grid.Grid(parent)

        self.myGrid.CreateGrid(0, 2, wx.grid.Grid.SelectRows)
        self.myGrid.EnableEditing(0)
        self.myGrid.SetRowLabelSize(30)
        self.myGrid.SetColLabelValue(0, 'Property')
        self.myGrid.SetColLabelValue(1, 'Type')
        self.myGrid.SetFont(wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.NORMAL, False, 'Sans'))
        self.myGrid.SetLabelFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL, False, 'Sans'))
        self.myGrid.SetSelectionBackground(wx.WHITE)
        self.myGrid.SetSelectionForeground(wx.BLACK)
        self.myGrid.DisableDragRowSize()

        self.myGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK, self.OnVariablesGridCellLeftClick)


    def OnVariablesGridCellLeftClick(self, event):
        row = event.GetRow()
        # if not self.Debug and (event.GetCol() == 0 and self.Table.GetValueByName(row, "Edit")):
        var_name = self.myGrid.GetCellValue(row, 0)
        var_class = "External"
        var_type = self.myGrid.GetCellValue(row, 1)
        data = wx.TextDataObject(str((var_name, var_class, var_type, "G::GLOBAL")))
        dragSource = wx.DropSource(self.myGrid)
        dragSource.SetData(data)
        dragSource.DoDragDrop()
        event.Skip()
