��          �      l      �  �   �     �     �     �     �  	   �     �     �     �      �          6     Q     h  %   �  $   �     �  )   �  +     $   D  �  i                   %     ,  	   2     <     D     J  "   g  &   �  (   �     �  -   �  0     /   N  &   ~  2   �  4   �  1   	                                                              
                       	             
An unhandled exception (bug) occured. Bug report saved at :
(%s)

Please be kind enough to send this file to:
beremiz-devel@lists.sourceforge.net

You should now restart program.

Traceback:
    External    InOut    Input    Local    Output    Temp  and %s  generation failed !
 "%s" Data Type doesn't exist !!! "%s" POU already exists !!! "%s" POU doesn't exist !!! "%s" can't use itself! "%s" config already exists! "%s" configuration already exists !!! "%s" configuration doesn't exist !!! "%s" data type already exists! "%s" element for this pou already exists! "%s" folder is not a valid Beremiz project
 "%s" is a keyword. It can't be used! Project-Id-Version: Beremiz
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Ibrahim KILICARSLAN <ibrahimhalilkilicarslan@outlook.com>, 2018
Language-Team: Turkish (Turkey) (https://www.transifex.com/beremiz/teams/75746/tr_TR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr_TR
Plural-Forms: nplurals=2; plural=(n > 1);
 
Beklenmeyen özel bir durum(hata) oluştu. Hata raporu kaydedildi:
(%s)
Bu raporu bize göndererek düzeltmemize yardımcı olun.
beremiz-devel@lists.sourceforge.net

Çalışmaya devam etmek için programı yeniden başlatmanız gerekmektedir.

Döndürülen hata:
 Harici Giriş-Çıkış Giriş Yerel Çıkış Geçici ve %s Derleme başarısız oldu !
 "%s" Veri türü mevcut değil !!! "%s" isminde bir program zaten var !!! "%s" isminde bir program bulunamadı !!! "%s" kullanamazsın! "%s" adında bir konfigürasyon zaten mevcut! "%s" adında bir konfigürasyon zaten mevcut !!! "%s" adında bir konfigürasyon bulunamadı !!! "%s" adında veri türü zaten mevcut! "%s"  öğe bu program için zaten kullanılmış! "%s" bu klasör geçerli bir Beremiz projesi değil
 "%s" bu bir anahtar kelimedir. Kullanamazsınız! 