��    �     T  �  �:      N  �   N     �N     �N     �N     �N  	   �N     O     O     O      )O     JO     fO     �O     �O  %   �O  $   �O     �O  )   P  +   HP  $   tP     �P     �P     �P  :   �P     *Q     CQ     ]Q  )   yQ  V   �Q  ;   �Q  !   6R  :   XR  9   �R     �R     �R     �R     �R     �R     �R     �R     �R     	S  
   S     S     :S     SS     `S     gS  
   vS     �S     �S     �S     �S  	   �S     �S     �S     �S     �S     �S  	   �S     �S      T     'T     BT     OT     TT  (   hT     �T  "   �T  	   �T     �T     �T     �T     �T     �T  V   �T      TU  .   uU     �U  )   �U  8   �U  ,   $V     QV     WV     gV     nV     {V     �V  "   �V     �V     �V     �V     �V  
   �V  4   �V     W     %W     2W     HW     OW  '   WW     W     �W     �W     �W     �W     �W     �W  
   X     X     X  	   %X     /X     8X     EX     NX     iX  	   {X     �X  +   �X  %   �X  7   �X  .   Y  ?   BY  
   �Y     �Y     �Y  
   �Y     �Y  
   �Y  6   �Y     �Y     �Y     Z     3Z     <Z  
   SZ  
   ^Z  
   iZ  	   tZ     ~Z     �Z     �Z  	   �Z     �Z     �Z  
   �Z     �Z     �Z     �Z     �Z  
   
[     [     [     #[     <[     M[     S[     r[     �[     �[     �[     �[     �[     �[     \     \     &\     6\     D\  
   Q\  
   \\     g\     w\  2   ~\  5   �\  "   �\  2   
]     =]  7   \]     �]     �]     �]  &   �]  +   �]  +    ^  !   L^  P   n^  2   �^     �^     _     _     '_     3_     F_     Y_     r_     �_  "   �_     �_     �_     �_     �_  )   
`  B   4`  >   w`     �`     �`     �`     �`     �`     �`     a     #a     0a     Fa     La     ^a  	   la     va     {a     �a     �a     �a     �a     �a  
   �a     �a  %   �a     b     #b     *b     ;b     Ib     Xb     xb     �b     �b  
   �b     �b     �b     �b      c     c  	   .c     8c     Dc     Lc     Uc     ]c     }c     �c     �c     �c     �c     �c     �c  #   �c  #   d  
   3d     >d  -   Ed     sd  3   �d  .   �d     �d     e     (e  
   =e     He     Ye     me     �e     �e     �e     �e     �e     �e     f  &   f     Af     [f     mf     �f     �f     �f     �f     �f     �f     �f     �f  
   g     g      g  	   /g  
   9g     Dg     Yg     gg     �g  $   �g     �g  =   �g     �g  	   
h     h  &   2h     Yh     bh     rh     yh     �h     �h     �h     �h     �h     �h     �h     i     i     )i  	   5i  
   ?i     Ji     Si  
   fi     qi     �i  
   �i     �i  +   �i     �i     �i     �i  %   �i  
   j     j     0j     Dj  	   Rj  !   \j     ~j     �j     �j  	   �j  	   �j     �j  9   �j     "k     1k     Ak     Pk     ak  
   vk     �k     �k     �k     �k     �k  %   �k  
   �k     l     l  N   l  &   `l  '   �l     �l     �l  L   �l     1m     Mm     bm     �m     �m     �m     �m     �m      �m      �m     n     (n     4n      =n     ^n     un     �n  %   �n     �n     �n     �n     �n      	o     *o     Do  	   Io     So     ao     oo     uo     �o  '   �o  )   �o  7   �o     "p     *p     3p     Cp     Rp     gp  +   wp  3   �p  	   �p     �p  /   �p     "q     *q     1q     Eq     Nq     [q     tq     �q     �q     �q     �q     �q     �q     �q     �q     �q  
   �q     �q  @   �q     ;r  1   Dr  1   vr  3   �r  	   �r     �r     �r     �r     	s     s     &s     /s     6s     <s     Ws     fs     ns  "   s  
   �s     �s  	   �s  	   �s     �s  .   �s     t  7   )t  6   at  %   �t  ,   �t  #   �t  )   u  -   9u     gu     }u     �u     �u     �u  3   �u     �u     �u  	   �u  C   �u     Cv     [v     vv     {v     �v  	   �v     �v  	   �v     �v     �v  
   �v     �v  
   �v     �v     �v     �v      w     -w     >w     Pw     Yw     nw  !   �w  :   �w     �w  "   �w  !   x     2x  8   9x     rx     �x     �x     �x  
   �x     �x     �x     �x     �x     �x     �x     �x     �x     y     y     y     /y     >y  
   Ly     Wy     hy  	   xy  P   �y     �y     �y  	   �y     �y     z     z     2z     Cz     Rz     _z     mz     uz     �z     �z     �z     �z     �z     �z     �z     �z     �z     �z     {     {     4{     Q{     U{     ^{  )   j{     �{  E   �{  (   �{  $   |  R   D|     �|     �|  2   �|  ?   �|     =}     V}     v}  +   }}  
   �}     �}  @   �}     �}     	~     ~  	   -~     7~     @~     N~     S~  �   a~  ,   �~               &     .     G     U     \     m     y     �     �     �      �     �  #   �  )   �     6�  9   D�     ~�     ��  	   ��     ��  	   ��     ��     ��  !   ΀  
   ��     ��     �     �     *�     1�  	   7�     A�     T�     `�     w�     ��     ��       '   ԁ  '   ��     $�  
   D�     O�     e�     u�     }�     ��     ��     ��  	   ��     ��     ͂     ނ     �     �      �     0�     L�  #   T�     x�  %   ��     ��     ��     ��     ˃     ؃     �     �     *�  9   =�     w�  
   ��     ��  #   ��     ΄     �     �     ��  	   �     �     �     #�     *�     7�     P�     W�  	   \�     f�     n�     ��     ��     ��     ��     ȅ     ۅ     �  
   ��     �     �     "�     ?�     O�     \�     h�     x�     �     ��     ��     ��     Æ     ن     �  
   ��  	   
�     �     �     (�     .�     >�     J�     V�     c�     t�  1   x�  6   ��  
   �  
   �     ��  =   ��  -   9�     g�  "   j�  %   ��     ��     ��     Ɉ  
   Έ     و  -   �     �     �     �     .�  
   7�     B�     [�     m�     ��  A   ��  	   Ӊ     ݉     �     �     �     &�     :�  	   >�  
   H�     S�  '   _�     ��  %   ��  	   ��     Ê     ܊     �     ��      �      �  O   )�     y�     ��  	   ��     ��     ċ     ҋ     ڋ     �     ��     ��     �     
�  	   �     %�  	   -�     7�     @�     L�     a�  
   t�     �  
   ��  8   ��     Ԍ     ٌ  
   �     �     �     ��  �   ��  E   �  5   ō  3   ��  H  /�  j   x�  k   �  7   O�  $   ��  �   ��  H   ;�  j   ��  m   �  C   ]�  B   ��  O   �  �   4�  m   ��  N   +�  q   z�  '   �  P   �  P   e�      ��     ו     ��     �     �     �     �     (�     9�     N�     f�     t�     ��     ��     ��  !   ��     ��  
   Ζ  R   ٖ     ,�     <�  I   M�  M   ��  &   �     �     �  
   &�     1�     F�     K�     ^�     ~�     ��     ��     ��  	   ��  	   ��  	   ��     ʘ  *   ט     �  )   �     I�     a�     f�  #   n�     ��  	   ��     ��     ��  	   Й     ڙ     �     �     ��     �     �     %�  "   4�  	   W�  	   a�  	   k�  1   u�  /   ��     ך     ��  '   ��     $�     A�     \�     t�     ��      ��     ʛ     ߛ     �     �     �     '�  '   /�     W�     e�     l�     r�  
   ~�     ��  
   ��     ��  8   ��  7   �  H   �  S   b�  9   ��     �     �     �     "�     (�     -�  
   2�  #   =�     a�     w�     ��     ��     ��     ��  *   ��  !   ޞ      �     �     �     �     #�     (�     0�     G�     _�     g�     ~�     ��     ��     ��     ��  	   ğ     Ο  &   ۟  :   �  s  =�  �   ��  	   q�     {�  	   ��  	   ��  	   ��     ��     ��     ��  &   Ȣ     �     �     '�     B�     Z�  #   x�     ��  &   ��  +   �  /   �     =�     T�     q�  J   ��  %   ٤     ��     �  #   4�  Z   X�  =   ��     �  4   �  0   B�     s�     ��     ��     ��     ��     ��     ��     ��     ��  
   ��     Ħ     ڦ     �     ��     �     �     �     $�     ,�     4�     <�     D�     L�     T�     \�     d�     r�      z�      ��     ��     Ч     ܧ     �  *   ��     $�  #   =�  
   a�     l�     ��     ��     ��     ��  d   ��  :   ��  1   1�     c�  $   ��  I   ��  0   �     "�  	   )�     3�     :�     P�     ]�  +   m�     ��  	   ��     ��     ��     ��  +   Ū     �     ��     �  	   $�  
   .�  !   9�  	   [�     e�     r�     ��     ��     ��  +   ī     �     ��     
�     �     $�     1�     >�     E�     X�     h�     o�  /   v�     ��  =   Ƭ  $   �  6   )�  	   `�  	   j�  	   t�     ~�  	   ��     ��  *   ��     ��     ȭ     �     ��     �     �  	   )�     3�  	   A�     K�  
   S�     ^�     }�  	   ��     ��     ��     ��     ��     Ԯ  	   خ  	   �     �     �     ��     �     �     �  !   9�     [�     m�     �     ��     ��     ��     ˯     ӯ     ܯ     �     ��  
   �  
   �     �     -�  0   4�  *   e�  $   ��  $   ��  '   ڰ  3   �     6�     H�     Z�  *   o�  /   ��  +   ʱ  '   ��  P   �  9   o�     ��     ��     ��     ۲     �     ��     �  	   !�     +�     A�     `�     s�     ��     ��  '   ��  K   �  E   /�     u�     |�  	   ��     ��     ��     ��     ɴ     ݴ     �     ��     �     �     �     +�     2�     9�     @�     M�     Z�     v�     ��     ��  &   ��     ǵ     ε     յ     �     �     �     �     �     &�     9�     @�     M�     ^�     p�     ��     ��  	   ��  	   ��     ��     ��     Ķ     �     �     ��     �     �     -�  	   4�  *   >�  *   i�     ��     ��  ,   ��     Է  1   �  +   #�     O�     n�     ��  
   ��     ��     ��  !   ĸ     �     ��     	�     �     /�     B�     X�     k�     ��     ��     ��     Ϲ     �     ��     �     �     (�     >�     E�  
   L�     W�     e�     s�     ��     ��     ��     ��     ĺ  *   Ѻ     ��  9   �     =�     J�     V�  -   r�  	   ��     ��     ��     Ż     ػ     �     ��     �     	�     �     ,�  	   3�     =�  	   J�     T�  	   [�  	   e�     o�     }�     ��  	   ��     ��     ��  '   ��     Ѽ     ؼ     ߼  &   �  	   �     �     '�     :�     G�  5   T�     ��     ��     ��     ƽ     ӽ     �  <   �     0�     C�     P�     `�     m�  	   ��     ��     ��     ��     ��     ��     ׾  	   ��     ��     �  F   �  #   T�  +   x�     ��     ��  O   ڿ     *�     G�     a�     |�     ��     ��     ��     ��  #   ��     ��     �     �  	   �     '�     G�     V�     e�  %   r�  	   ��  (   ��  	   ��  "   ��  "   ��     �     7�     >�     N�     ^�  	   k�  	   u�     �  -   ��  '   ��  9   ��  	   �     )�     0�  	   <�     F�  	   V�  !   `�  '   ��     ��     ��  .   ��  	   ��  	   ��     �     �     �     �     /�     B�  	   W�  	   a�  	   k�  	   u�  
   �     ��     ��     ��  
   ��     ��  :   ��  	   �  (   �  '   6�  (   ^�  	   ��  	   ��     ��  	   ��     ��  	   ��     ��     ��     ��     ��     ��  	   ��     �  !   �  	   -�     7�     D�     K�     R�  *   Y�     ��  6   ��  6   ��     �  '   1�     Y�  (   u�  .   ��     ��     ��     ��  	   ��     �  4   �     C�     J�  	   `�  @   j�     ��     ��     ��     ��     ��     �     �     �     !�     %�     ,�     3�  
   ;�     F�     L�     S�     `�     }�     ��     ��     ��     ��      ��  *   ��     �     !�     A�     Z�  =   a�     ��  	   ��     ��     ��     ��     ��     ��  	   �     �  	   �  	   !�     +�     8�     ?�     F�     T�     g�     w�  	   ��     ��     ��  	   ��  :   ��     ��     	�     �     �     *�     7�     D�     Q�     ^�     k�     x�     �     ��     ��     ��     ��     ��     ��     ��     ��  	   ��     ��     �     �     .�     K�     R�  	   _�  .   i�  +   ��  :   ��     ��  !   �  G   >�     ��     ��  7   ��  D   ��     A�     `�     �  *   ��  	   ��     ��  ?   ��  	   �  	   �     �  	   %�     /�     <�     I�     P�  `   `�  0   ��     ��     �     �     �     %�     2�     9�  
   O�  
   Z�     e�  
   m�     x�  %   ��     ��      ��  -   ��     �  >   0�     o�  
   v�     ��  	   ��     ��     ��     ��     ��     ��     ��     �  	   �     !�     .�  	   5�     ?�     O�     \�     r�     ��     ��     ��  '   ��  -   ��     �     9�     F�     Y�     f�  	   s�     }�     ��     ��  	   ��     ��     ��     ��     ��     ��     �     �     9�     @�     _�  !   f�     ��     ��     ��     ��     ��     ��     ��     �  3   �  	   O�     Y�     `�     m�     ��     ��     ��     ��     ��     ��     ��  	   ��     ��     �     "�     *�     1�     8�     ?�     O�  	   _�     i�     y�     ��     ��     ��  	   ��     ��     ��     ��     ��     	�     �     #�  	   0�     :�     G�     ^�     e�     l�     �     ��     ��     ��     ��     ��     ��     ��  	   ��     ��     ��     �     �  (   �  )   B�     l�     y�     ��  7   ��  ,   ��     ��  &   �  &   2�     Y�     _�     l�     s�  	   ��  %   ��     ��     ��     ��     ��     ��     ��     �     �     *�  4   =�     r�     y�     ��     ��     ��     ��     ��  	   ��     ��     ��  '   ��     �  $   �     9�     F�     S�     `�     g�     k�  )   t�  N   ��     ��     �  	   �     !�     2�  	   ?�  
   I�     T�     d�     h�  	   o�     y�  	   ��  	   ��  	   ��  	   ��     ��     ��     ��     ��     ��  
   ��  -   �     0�     7�     >�     K�  	   S�     ]�  `   d�  F   ��  )   �  )   6�    `�  E   q�  O   ��  4   �     <�  k   [�  3   ��  v   ��  q   r�  ?   ��  E   $�  E   j�  u   ��  T   &�  H   {�  n   ��  *   3�  [   ^�  :   ��     ��     �     �     "�     /�     <�     I�     V�     c�     v�     ��  	   ��     ��     ��  	   ��     ��     ��     ��  H   ��     '�     4�  =   D�  =   ��  %   ��     ��     ��     ��     �     �     �     .�     I�     V�     f�  	   s�  
   }�  
   ��     ��  
   ��  &   ��     ��  +   ��     �     1�     8�  !   ?�  	   a�     k�     w�     ��     ��     ��     ��     ��     ��  
   ��     ��     ��      �  	   �     &�     -�  ,   :�  ,   g�     ��     ��  !   ��     ��     ��     �     �     8�     I�     h�     |�     ��     ��     ��     ��  (   ��     �  	   �     �  	   #�     -�     4�  
   @�     K�  <   S�  ;   ��  O   ��  E   �  9   b�     ��     ��     ��     ��     ��     ��     ��  #   
�     .�     D�     Z�  	   a�     k�  	   r�  .   |�     ��     ��     ��     ��     ��     ��     ��     �     �     0�     4�     J�     ]�     p�     w�     ��     ��     ��  -   ��  5   ��     �      �            B  �   c  �       �      O        T                         q    �   5  �    �  �   �  z     �      �  w      *   �  k  �   �                    =              �  M      v      �   J      �   �  ~  ]  �                               D  i  �  7  k       T      E  ,   L  �        +  �  Y    ,  +  S  �   �  9       z  �               �  ^  (  K     O   �         g        �  �   �             R  �   Q   �   �   ^  �   5   W           3      �   �          !   a      "    A  ^  �  �   �  r   ;  l         %  �   �      �       �      �  �  �   �          �   �      �           2      G  F      �      M      $   �   �      Q  �  e  �  �       �  �  ?  �  !     .  �       �  V  '  �  �   @  [  �   �      2      "   j  �  \   �      _              �   �  8           �  d       �  
  j   |  �      �   �      z  �  $    H  >  �   �   �   �      �       �  6  e        }  b        a      �  �   �      l  �   �          #  	  �  �      �  �  �  y  �       )  Y         �   N   e  '  Y  ,  �  ]  �     {  N          �   F  �  �  �  w  �  �  J      �      �   r       0   �   R  �  �  �  g   �  �                d        �                !      �   4  d  �     �          �  S      O  �    3   �       �          �   �           �  P      �   �       �  �  �       �         _      \  ;  s   �  <  �      3  �   *  ~       �  �  �      x      �    �  (   q   p  �  T   $  s            q  �       0  �   t   
   �  q      �      G      n   V      �   �  6  �       �  #      �      �  �   g              �   X  �  [          }  8      �  �  �      �  9      �      �   �     �  �  *  '  d      [          �       �  i        8  m  �      D   �          �    	      �   D  �   w  �  �  �  M  4   Q  �  =   \  �  �        #   h  D             �  E  �     �  B   H     �   �  v   �  C   Z    l  @  9  �   �  z            6   �  �  /      �   �  &  �      @  �  9  R       M   x              �      A  �      �  )  �  �    w               G  0  i         K  �  �   �  ,    �              �  �        �    R  �   �      *  �  �       �          �      �  8      �      �  )    �          �      �      ;       "  >  �      �       �    �  �         �   ?       _  �   �   A      E       7  �     	   }      Y  Z  B  }      �   c  1   �   E            �    �   t  -         �  �  1  �   �                  v  r  �      �  {  h   �  �  f   .  �      -      �  5  s              �  V  �  �  �  �        �      0  C  F   m   �  �   �      m      y   n  �    �  {  �   �  �   �   o          H  u       P   ?  �       �  �  ~  m  �   �   �  �   a   �            �  r  u                P  �  �   T      C  &  �  �  �    ?              �  `  �          o          �  �    �  �  +   �   7   �   _  �          N  �  �   �     �  �  �    h  c  i    	  >   V       �  k  �       <      b  �  �  \        �      |      #  �      X  f  �  �  �           �  �    ^   �       b   1      `  o  .   �   I  �   �      �  O          �  �   �  @   �          A   U   |           I   �  e  �  J   s              K       �  �  �       L   F          N  �      �  t  C  �  4  �  x  /  �      p   <      �             p    �  ]     �  �  X     3  �   =  �       �      �   �  n    U  6  %  �              �  `  p              �  �      �  5      /  f    �  �   k  �       Z                  �      t  �  L  2  �   �     W  �          �  ~  x             %  �    �     �      �        �  �  �  (        �  �   
  h  U  �       �  �  �  �      4  S  �  +  �     u          �  �   =  �             :  ]  �  |  �   .               �  "  �   �  (      7  �  �       
  �  y          �   U          n         �       �   $  <   �     �  �                      b  :  Q  `   �  2   �  Z   �   -  �  W  �  �  j  &    :  �         �   �  �        J  �  W  �      X  !  �  S        �           �  �  �  :        �      �     �  K  I  �  B  �        >  �  G   �     �          �  �              �  g      �  �  l  �   �      �   �   �   �     �   v    �  �       �  {           %   �  �      &   1  H          '       f  �  �      )   �        P      �  �  �   I      -   o       /   �  [      a  �       �      �                                �  �    �  �  j  u          �  �          �   y  L      �  �  �   ;  c    
An unhandled exception (bug) occured. Bug report saved at :
(%s)

Please be kind enough to send this file to:
beremiz-devel@lists.sourceforge.net

You should now restart program.

Traceback:
    External    InOut    Input    Local    Output    Temp  and %s  generation failed !
 "%s" Data Type doesn't exist !!! "%s" POU already exists !!! "%s" POU doesn't exist !!! "%s" can't use itself! "%s" config already exists! "%s" configuration already exists !!! "%s" configuration doesn't exist !!! "%s" data type already exists! "%s" element for this pou already exists! "%s" folder is not a valid Beremiz project
 "%s" is a keyword. It can't be used! "%s" is an invalid value! "%s" is not a valid folder! "%s" is not a valid identifier! "%s" is used by one or more POUs. Do you wish to continue? "%s" pou already exists! "%s" step already exists! "%s" value already defined! "%s" value isn't a valid array dimension! "%s" value isn't a valid array dimension!
Right value must be greater than left value. "{a1}" function cancelled in "{a2}" POU: No input connected "{a1}" is already used by "{a2}"! "{a1}" resource already exists in "{a2}" configuration !!! "{a1}" resource doesn't exist in "{a2}" configuration !!! %(codefile_name)s %03gms %dd %dh %dm %dms %ds %s Data Types %s POUs %s Profile %s body don't have instances! %s body don't have text! &Add Element &Close &Configuration &Data Type &Delete &Display &Edit &File &Function &Help &License &Program &Properties &Recent Projects &Resource '{a1}' - {a2} match in project '{a1}' - {a2} matches in project '{a1}' is located at {a2}
 (%d matches) , %s - Select URI type - 0 - current time, 1 - load time from PDT 0 - manual , 1 - automatic 0 - track X0, 1 - ramp to/track X1 0 = reset 1 = integrate, 0 = hold 1d 1h 1m 1s A POU has an element named "%s". This could cause a conflict. Do you wish to continue? A POU named "%s" already exists! A child named "{a1}" already exists -> "{a2}"
 A location must be selected! A task with the same name already exists! A variable with "%s" as name already exists in this pou! A variable with "%s" as name already exists! About Absolute number Action Action Block Action Name Action Name: Action with name %s doesn't exist! Actions Actions: Active Add Add Action Add C code accessing located variables synchronously Add Configuration Add DataType Add Divergence Branch Add IP Add POU Add Python code executed asynchronously Add Resource Add Transition Add Wire Segment Add a new initial step Add a new jump Add a new step Add a simple WxGlade based GUI. Add action Add element Add instance Add slave Add task Add variable Addition Additional function blocks Adjust Block Size Alignment All All files (*.*)|*.*|CSV files (*.csv)|*.csv Already connected. Please disconnect
 An element named "%s" already exists in this structure! An instance with the same name already exists! Apply name modification to all continuations with the same name Arc cosine Arc sine Arc tangent Arithmetic Array Assignment At least a variable or an expression must be selected! Author Author Name (optional): BUSY = 1 during ramping period Backward Bad location size : %s Base Type: Base Types BaseParams Baud_Rate Beremiz BeremizRoot Binary selection (1 of 2) Bit-shift Bitwise Bitwise AND Bitwise OR Bitwise XOR Bitwise inverting Block Block Properties Block name Bottom Broken Browse %s values library Browse Locations Build Build directory already clean
 Build project into build folder C Build crashed !
 C Build failed.
 C code C code generated successfully.
 C compilation failed.
 C compilation of %s failed.
 C extension C&redits CANOpen network CANOpen slave CAN_Baudrate CAN_Device CAN_Driver CANopen support CFLAGS Can only generate execution order on FBD networks! Can only give a location to local or global variables Can't generate program to file %s! Can't give a location to a function block instance Can't save project to file %s! Can't set an initial value to a function block instance CanFestivalInstance CanFestivalNode CanFestivalSlaveNode Cannot create child {a1} of type {a2}  Cannot find lower free IEC channel than %d
 Cannot get PLC status - connection failed.
 Cannot open/parse VARIABLES.csv!
 Cannot set bit offset for non bool '{a1}' variable (ID:{a2},Idx:{a3},sIdx:{a4})) Cannot transfer while PLC is running. Stop it now? Case sensitive Center Change IP of interface to bind Change Name Change POU Type To Change Port Number Change working directory Character string Choose a SVG file Choose a directory to save project Choose a file Choose a project Choose a value for %s: Choose a working directory  Choose an empty directory for new project Chosen folder doesn't contain a program. It's not a valid project! Chosen folder isn't empty. You can't use it for a new project! Class Class Filter: Class: Clean Clean log messages Clean project build folder Cleaning the build directory
 Clear Errors Clear Execution Order Close Close Application Close Project Close Tab Coil Command Comment Community support Company Name Company Name (required): Company URL (optional): Comparison Compiler Compiling IEC Program into C code...
 Concatenation Config Config variables Configuration Configurations Confirm or change variable name Connect Connect to the target PLC Connected to URI: %s Connection Connection Properties Connection canceled!
 Connection failed to %s!
 Connection lost!
 Connection to '%s' failed.
 Connector Connectors: Console Constant Contact Content Description (optional): Continuation Conversion from BCD Conversion to BCD Conversion to date Conversion to time-of-day Copy Copy POU Copy file from left folder to right Copy file from right folder to left Copy of IN Cosine Could not add child "{a1}", type {a2} :
{a3}
 Couldn't import old %s file. Couldn't load confnode base parameters {a1} :
 {a2} Couldn't load confnode parameters {a1} :
 {a2} Couldn't paste non-POU object. Couldn't start PLC !
 Couldn't stop PLC !
 Create HMI Create a new POU Create a new action Create a new action block Create a new block Create a new branch Create a new coil Create a new comment Create a new connection Create a new contact Create a new divergence Create a new divergence or convergence Create a new initial step Create a new jump Create a new power rail Create a new rung Create a new step Create a new transition Create a new variable Credits Current working directory : Cut Cyclic DEPRECATED DS-301 Profile DS-302 Profile Data Type Data Types Data type conversion Date addition Date and time subtraction Date subtraction Datetime, current or relative to PDT Days: Debug does not match PLC - stop/transfert/start to re-enable
 Debug instance Debug: %s Debug: Unknown variable '%s'
 Debug: Unsupported type to debug '%s'
 Debugger Debugger ready
 Delete Delete Divergence Branch Delete File Delete Wire Segment Delete item Deletion (within) Derivation Type: Derivative time constant Description Description: Differentiated output Dimensions: Direction Direction: Directly Disable_Extensions Disconnect Disconnect from PLC Disconnected Divergence Division Do you really want to delete the file '%s'? Documentation Done Duration EDS files (*.eds)|*.eds|All files|*.* Edit Block Edit Coil Values Edit Contact Values Edit Duration Edit Step Edit a WxWidgets GUI with WXGlade Edit action block properties Edit array type properties Edit comment Edit file Edit item Edit jump target Edit raw IEC code added to code generated by PLCGenerator Edit step name Edit transition Editor ToolBar Editor selection Elapsed time of ramp Elements : Empty Empty dimension isn't allowed. Enabled Enter a name  Enter a port number  Enter the IP of the interface to bind Enumerated Equal to Error Error : At least one configuration and one resource must be declared in PLC !
 Error : IEC to C compiler returned %d
 Error in ST/IL/SFC code generator :
%s
 Error while saving "%s"
 Error: Export slave failed
 Error: Modbus/IP Servers %{a1}.x and %{a2}.x use the same port number {a3}.
 Error: No Master generated
 Error: No PLC built
 Exception while connecting %s!
 Execution Control: Execution Order: Experimental web based HMI Exponent Exponentiation Export CanOpen slave to EDS file Export graph values to clipboard Export slave Expression: External Extracting Located Variables...
 FB for derivative term FB for integral term FBD Failed : Must build before transfer.
 Falling Edge Fatal : cannot get builder.
 Fetching %s Field %s hasn't a valid value! Fields %s haven't a valid value! File '%s' already exists! Find Find Next Find Previous Find position Find: Force value Forcing Variable Value Form isn't complete. %s must be filled! Form isn't complete. Name must be filled! Form isn't complete. Valid block type must be selected! Forward Function Function &Block Function Block Function Block Types Function Blocks Function Blocks can't be used in Functions! FunctionBlock "%s" can't be pasted in a Function!!! Functions Generate Program Generating SoftPLC IEC-61131 ST/IL/SFC code...
 Generic Global Go to current value Graphics Greater than Greater than or equal to Grid Resolution: HTTP interface port : Height: Home Directory: Horizontal: Hours: IEC_Channel IL IP IP is not valid! Import SVG InOut InOut variable {a1} in block {a2} in POU {a3} must be connected. Inactive Incompatible data types between "{a1}" and "{a2}" Incompatible size of data between "%s" and "BOOL" Incompatible size of data between "{a1}" and "{a2}" Indicator Initial Initial Step Initial Value Initial Value: Initial value Inkscape Inline Input Input to be differentiated Input variable Inputs: Insertion (into) Instance with id %d doesn't exist! Instances: Integrated output Interface Interrupt Interval Invalid URL!
Please enter correct URL address. Invalid plcopen element(s)!!! Invalid type "{a1}"-> {a2} != {a3}  for location "{a4}" Invalid type "{a1}"-> {a2} != {a3} for location "{a4}" Invalid value "%s" for debug variable Invalid value "%s" for variable grid element Invalid value "%s" for viewer block Invalid value "{a1}" for "{a2}" variable! Invalid value!
You must fill a numeric value. Invocation_Rate_in_ms Is connection secure? Jump LD LDFLAGS Ladder element with id %d is on more than one rung. Language Language (optional): Language: Latest build already matches current target. Transfering anyway...
 Launch WX GUI inspector Launch a live Python shell Left Left PowerRail Length of string Less than Less than or equal to Libraries Library License Limitation Linker Linking :
 Linux Local Local entries Local service discovery failed!
 Local_IP_Address Local_Port_Number Location Locations available: Logarithm to base 10 MDNS resolution failure for '%s'
 Manual output adjustment - Typically from transfer station Map Variable Map located variables over CANopen Map located variables over Modbus Master Max count ({a1}) reached for this confnode of type {a2}  MaxRemoteTCPclients Maximum Maximum: Memory MemoryArea MemoryAreaType Menu ToolBar Microseconds: Middle Milliseconds: Minimum Minimum: Minutes: Miscellaneous Modbus support ModbusRTUclient ModbusRTUslave ModbusRequest ModbusRoot ModbusServerNode ModbusTCPclient Modifier: More than one connector found corresponding to "{a1}" continuation in "{a2}" POU Move action down Move action up Move down Move element down Move element up Move instance down Move instance up Move task down Move task up Move the view Move up Move variable down Move variable up Multiplexer (select 1 of N) Multiplication My Computer: NAME NOT R1 Name Name must not be null! Name: Natural logarithm Negated Nevow Web service failed.  Nevow/Athena import failed : New New item No Modifier No PLC to transfer (did build succeed ?)
 No body defined in "%s" POU No connector found corresponding to "{a1}" continuation in "{a2}" POU No documentation available.
Coming soon. No informations found for "%s" block No output {a1} variable found in block {a2} in POU {a3}. Connection must be broken No search results available. No such SVG file: %s
 No such index/subindex ({a1},{a2}) (variable {a3}) No such index/subindex ({a1},{a2}) in ID : {a3} (variable {a4}) No valid value selected! No variable defined in "%s" POU NodeId Non existing node ID : {a1} (variable {a2}) Non-Retain Normal Not PDO mappable variable : '{a1}' (ID:{a2},Idx:{a3},sIdx:{a4})) Not equal to Nr_of_Channels Number of sequences: Numerical OnChange Only Elements Open Open Inkscape Open Source framework for automation, implemented IEC 61131 IDE with constantly growing set of extensions and flexible PLC runtime. Open a file explorer to manage project files Open wxGlade Option Options Organization (optional): Other Profile Output Overriding reset PDO Receive PDO Transmit PLC :
 PLC Log PLC code generation failed !
 PLC is empty or already started. PLC is not started. PLC syntax error at line {a1}:
{a2} PLCOpen files (*.xml)|*.xml|All files|*.* PLCOpenEditor PLCOpenEditor is part of Beremiz project.

Beremiz is an  PORT POU Name POU Name: POU Type POU Type: PV - SP PYRO connecting to URI : %s
 PYRO using certificates in '%s' 
 Page Setup Page Size (optional): Page: %d Parent instance Parity Paste Paste POU Pattern to search: Pin number: Please choose a target Please enter a block name Please enter comment text Please enter step name Please enter text Please enter value for a "%s" variable: Port number must be 0 <= port <= 65535! Port number must be an integer! Power Rail Power Rail Properties Preset datetime Preview Preview: Print Print preview Priority Priority: Problem starting PLC : error %d Process variable Product Name Product Name (required): Product Release (optional): Product Version Product Version (required): Program Program was successfully generated! Programs Programs can't be used by other POUs! Project Project '%s': Project Files Project Name Project Name (required): Project Version (optional): Project file syntax error:

 Project properties Project tree layout do not match confnode.xml {a1}!={a2}  Propagate Name Properties Proportionality constant Publishing service on local network Pyro exception: %s
 Pyro port : Python code Python file Qualifier Quit Ramp duration Range: Raw IEC code Really delete node '%s'? Realm: Redo Reference Refresh Regular expression Regular expressions Release value Remainder (modulo) Remote_IP_Address Remote_Port_Number Remove %s node Remove Datatype Remove Pou Remove action Remove element Remove file from left folder Remove instance Remove slave Remove task Remove variable Rename Replace File Replace Wire by connections Replacement (within) Reset Reset Execution Order Reset Perspective Reset search result Reset time Resources Retain Return Type: Right Right PowerRail Rising Edge Rotate left Rotate right Rounding up/down Run Runtime IO extensions C code generation failed !
 Runtime library extensions C code generation failed !
 SDO Client SDO Server SFC SFC jump in pou "{a1}" refers to non-existent SFC step "{a2}" SFC transition in POU "%s" must be connected. ST ST files (*.st)|*.st|All files|*.* SVG files (*.svg)|*.svg|All files|*.* SVGUI Sampling period Save Save As... Save as Save path is the same as path of a project! 
 Scope Search Search in Project Seconds: Select All Select a variable class: Select an editor: Select an instance Select an object Selected directory already contains another project. Overwrite? 
 Selection Selection Convergence Selection Divergence Serial_Port Service Discovery Services available: Set Set point Shift left Shift right Show IEC code generated by PLCGenerator Show Master Show Master generated by config_utils Show code Simultaneous Convergence Simultaneous Divergence Sine Single SlaveID Source didn't change, no build.
 Source signal has to be defined for single task '{a1}' in resource '{a2}.{a3}'. Square root (base 2) Standard function blocks Start PLC Start build in %s
 Start_Address Started Starting PLC
 Status ToolBar Step Stop Stop PLC Stop Running PLC Stop_Bits Stopped Structure Subrange Subtraction Successfully built.
 Switch perspective Sync_Align Sync_Align_Ratio Sync_TPDOs Syntax error in regular expression of pattern to search! TYPE Tangent TargetType Task Tasks: Temp The PID (proportional, Integral, Derivative) function block provides the classical three term controller for closed loop control. The RAMP function block is modelled on example given in the standard. The RS bistable is a latch where the Reset dominates. The SR bistable is a latch where the Set dominates. The best place to ask questions about Beremiz/PLCOpenEditor
is project's mailing list: beremiz-devel@lists.sourceforge.net

This is the main community support channel.
For posting it is required to be subscribed to the mailing list.

You can subscribe to the list here:
https://lists.sourceforge.net/lists/listinfo/beremiz-devel The derivative function block produces an output XOUT proportional to the rate of change of the input XIN. The down-counter can be used to signal when a count has reached zero, on counting down from a preset value. The file '%s' already exist.
Do you want to replace it? The group of block must be coherent! The hysteresis function block provides a hysteresis boolean output driven by the difference of two floating point (REAL) inputs XIN1 and XIN2. The integral function block integrates the value of input XIN over time. The off-delay timer can be used to delay setting an output false, for fixed period after input goes false. The on-delay timer can be used to delay setting an output true, for fixed period after an input becomes true. The output produces a single pulse when a falling edge is detected. The output produces a single pulse when a rising edge is detected. The pulse timer can be used to generate output pulses of a given time duration. The real time clock has many uses including time stamping, setting dates and times of day in batch reports, in alarm messages and so on. The semaphore provides a mechanism to allow software elements mutually exclusive access to certain resources. The up-counter can be used to signal when a count has reached a maximum value. The up-down counter has two inputs CU and CD. It can be used to both count up on one input and down on the other. There are changes, do you want to save? There is a POU named "%s". This could cause a conflict. Do you wish to continue? There was a problem printing.
Perhaps your current printer is not set correctly? This option isn't available yet! Tick: %d Time Time addition Time concatenation Time division Time multiplication Time subtraction Time-of-day addition Time-of-day subtraction Timeout_in_ms Toggle value Top Transfer Transfer PLC Transfer completed successfully.
 Transfer failed
 Transition Transition "%s" body must contain an output variable or coil referring to its name Transition Name Transition Name: Transition with content "{a1}" not connected to a next step in "{a2}" POU Transition with content "{a1}" not connected to a previous step in "{a2}" POU Transition with name %s doesn't exist! Transitions Translated by Triggering Twisted unavailable. Type Type and derivated Type conflict for location "%s" Type conversion Type infos: Type strict Type: URI host: URI port: URI type: URI_location Unable to define PDO mapping for node %02x Unable to get Xenomai's %s 
 Undefined block type "{a1}" in "{a2}" POU Undefined pou type "%s" Undo Unknown Unknown variable "%s" for this POU! Unnamed Unnamed%d Unrecognized data size "%s" User Data Types User Type User-defined POUs Value Values: Variable Variable Drop Variable Properties Variable class Variable don't belong to this POU! Variable: Variables Vertical: WAMP Client connection failed (%s) .. retrying .. WAMP Client connection lost (%s) .. retrying .. WAMP ID: WAMP client connecting to : WAMP client connection not established! WAMP client startup failed.  WAMP config is incomplete. WAMP config is missing. WAMP connecting to URL : %s
 WAMP connection timeout WAMP connection to '%s' failed.
 WAMP import failed : WAMP load error:  WAMP session left WXGLADE GUI Wamp secret load error: Warning Warnings in ST/IL/SFC code generator :
 Whole Project Width: Win32 Wrap search Written by WxGlade GUI XenoConfig Xenomai You don't have write permissions.
Open Inkscape anyway ? You don't have write permissions.
Open wxGlade anyway ? You must have permission to work on the project
Work on a project copy ? You must select the block or group of blocks around which a branch should be added! You must select the wire where a contact should be added! You must type a name! You must type a value! Zoom class days desc error: %s
 exited with status {a1} (pid {a2})
 first input parameter first output parameter function functionBlock hours initial internal state: 0-reset, 1-counting, 2-set matiec installation is not found
 milliseconds minutes name onchange opts program second input parameter second output parameter seconds string from the middle string left of string right of type update info unavailable. variable variables warning: %s
 {a1} "{a2}" can't be pasted as a {a3}. {a1} XML file doesn't follow XSD schema at line {a2}:
{a3} Project-Id-Version: Beremiz
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: yiwei Yan <523136664@qq.com>, 2017
Language-Team: Chinese (China) (https://www.transifex.com/beremiz/teams/75746/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 
未处理的异常发生(错误) 。缺陷报告保存在：
(%s)

请好心地把这个文件发送到：
beremiz-devel@lists.sourceforge.net

您现在应该重新启动程序.

追溯：
    外部    输入输出    输入    本地    输出 临时 和 %s 生成失败！
 "%s" 数据类型尚不存在！！！ "%s"POU已经存在！！！ "%s" POU不存在！！！ "%s" 不能自己使用！ "%s" 配置已存在！ "%s" 配置已存在！！！ “ %s ” 配置不存在！！！ "%s" 数据类型已存在！ "%s" 元素对于此POU已经存在！ "%s" 文件夹不是有效的Beremiz项目
 "%s" 是一个关键词。它不能被使用！ "%s"不是有效值！ "%s"不是有效文件夹！ "%s"不是有效标识符！ "%s"被一个或多个程序组织单元使用，你确定要继续吗？  "%s"编程组织单元已经存在！ "%s"步骤已经存在！ "%s" 值已经被定义！ "%s" 值不是有效数组维数！ "%s" 不是一个有效的数组维数值！
右边的数值必须大于左边的数值。 "{a1}" 功能 被取消在 "{a2}" POU：没有输入被连接 "{a1}" 被用在了 "{a2}"! "{a1}" 资源已经存在于 "{a2}" 配置 ！！！ "{a1}" 资源部存在于 "{a2}" 配置！！！ %(codefile_name)s %03gms %dd %dh %dm %dms %ds %s 数据类型 %s POUs %s Profile %s 未包含实例！ %s 未包含文本！ &增加元素 &关闭 &配置 &数据类型 &删除 &显示 &编辑 &文件 &功能 &帮助 &许可 &程序 &属性 &最近项目 &资源 '{a1}' - {a2} 在项目中匹配 '{a1}' - {a2} 在项目中匹配 '{a1}' 位于 {a2}
 (%d 匹配) ， %s - 选择 URI 类型 - 0 - 当前时间， 1 - 从PDT下载时间 0 - 手动， 1 - 自动 0 - 追踪X0， 1 - 斜坡/追踪X1 0 = 复位 1 = 积分， 0 = 保持 1d 1h 1m 1s 一个编程组织单元的成员被命名为"%s"。这可能会产生冲突。你希望继续吗？ 一个以"%s"命名的的编程组织单元已经存在！ 一个子命名的 "{a1}" 已经存在 -> "{a2}"
 一个定位必须被选择！ 相同名称的任务已经存在！ 一个以"%s"命名的变量在这个编程组织单元中已经存在！ 一个变量以 "%s" 作为名字已经存在！ 关于 绝对值 动作 动作控制功能块 动作名字 动作名字： 一个以"%s"命名的的行动不存在！ 动作 动作： 活动 添加 添加动作 同步的增加C代码访问定位的变量 添加配置 添加数据类型 添加发散分支 添加 IP 添加 POU 添加异步执行的Python代码 添加源 添加跃迁 添加布线段 新建一个初始步骤 新建一个跳跃 添加一个新步骤 添加一个简单的基于 WxGlade的 GUI. 添加动作 添加元素 添加实例 添加从站 添加任务 添加变量 加法 附加功能类型 调整块尺寸 对准 所有 所有文件 (*.*)|*.*|CSV 文件 (*.csv)|*.csv 已经连接。请断开连接
 一个以"%s"命名的元素已经在这个结构中存在！ 相同名称的实例已经存在！ 应用名称修改到所有伴随相同名称的延续 反余弦 反正弦 反正切 运算 阵列的 分配 至少选择一个变量或者表达式！ 作者 作者姓名（选填）： BUSY = 1 在加速期 反向 不好的位置大小：%s 基类型： 基类型 基本参照  波特率 Beremiz Beremiz根 二进制选取（二选一） 位移 位操作 按位”与“ 按位”或“ 按位”异或“ 按位“反向” 块 块属性 块名称 底部 损坏 浏览 %s 值库 浏览定位 构建 构建目录已经清除
 在构建文件夹中构建项目 C构建损坏！
 C构建失败。
 C 代码 C代码生成成功。
 C编译失败。
  %s 的C编译失败。
 C扩展 C&redits CANOpen 网络 CANOpen 从站 CAN_波特率 CAN_设备 CAN_驱动 CANOpen 支持 CFLAGS 在功能块网络，只能生成执行命令！ 只能影响本地或全局变量的位置 这个编程生成文件失败 %s！ 不能影响功能块实例的位置 这个项目保存为文件失败 %s！ 不能设置一个初始值到一个功能块实例 CanFestival实例 CanFestival节点 CanFestival从节点 不能创建类型 {a2} 的子类型 {a1}  无法找到比 %d 更低的自由的IEC通道
 无法获取PLC的状态 - 连接失败。
 无法打开／解析 VARIABLES.csv！
 不能设置位偏移对于非布尔 '{a1}' 变量 (ID:{a2},Idx:{a3},sIdx:{a4})) 在PLC运行时，不能传输！现在停止运行吗？ 区分大小写 中 更改界面的ip用以绑定 更改名字 将POU类型转换为 更改端口号 更改工作目录 字符串 选择一个SVG文件 选择一个目录保存项目 选择一个文件 选择一个项目 这对 %s选择一个值： 选择一个工作目录 选择一个空白目录以保存项目 被选中的文件夹未包含一个程序。它不是一个有效项目！ 被选中的文件夹非空。你不能用它创建一个新项目！ 分类 类过滤器： 分类： 清除 清楚记录信息 清除项目构建目录 清除构建目录
 清楚错误 清空执行命令 关闭 关闭应用 关闭项目 关闭标签 线圈 命令 注释 社区支持 公司名称 公司名字（必须）： 公司网址（选填）： 比较 编译 正在将IEC程序编译成C代码...
 级联 配置 配置变量 配置 配置 确认或变更变量名称 连接 连接到PLC目标 连接到URI： %s 连接 连接属性 取消连接！
 连接失败 %s!
 失去连接！
 连接到 '%s' 失败。
 连接 连接： 控制台 常量 连接 内容描述（选填）： 连续 由BCD码转换 转换为BCD码 转换为日期 转换为日期时间 复制 复制POU 从左侧文件夹中复制文件到右侧 从右侧文件夹中复制文件到左侧 IN的复制 余弦 不能增加子 "{a1}"，类型 {a2} :
{a3}
 不能导入旧 %s 文件。 不能加载 confnode 基础参数 {a1} ：
 {a2} 不能加载 confnode 参数 {a1} ：
 {a2} 不能粘贴 非-POU 目标。 无法开始PLC！
 无法停止PLC！
 新建 HMI 新建一个POU 新建一个动作 新建一个动作控制功能块 新建一个块 新建一个支流 新建一个线圈 新建一个备注 新建一个连接 新建一个接触点 新建一个发散 新建一个发散或者收敛 新建一个初始步骤 新建一个跳跃 新建一个电源导轨 新建一个梯级 新建一个步骤 新建一个跃迁 新建一个变量 关于作者 当前工作目录： 剪切 循环 DEPRECATED DS-301 配置 DS-302 配置 数据类型 数据类型  日期类型转换 日期加法 日期和时间减法 日期减法 日期时间，当前的或者PDT相对的 日： 调试部匹配PLC  -  停止/传输/启动 来新启用
 调试实例 调试：%s 调试：未知变量 '%s'
 调试：不支持的类型进行调试 '%s'
 调试器 调试器准备好
 删除 删除发散分支 删除文件 删除布线段 删除项目 删除 推导类型： 微分时间常数 描述 描述： 微分输出 维数： 方向 方向： 直接的 禁止_扩展 断开 从PLC断开 已断开 偏差 除法 你真的想删除这个文件 '%s' ？ 文档 完成 时间 EDS 文件 (*.eds)|*.eds|All files|*.* 编辑块 编辑线圈值 编辑接触点值 编辑期间 编辑步骤 用 WXGlade 编辑一个 WxWidgets 用户图形界面 编辑行动块属性 编辑数组类型属性 编辑注释 编辑文件 编辑项目 编辑跳跃目标 编辑原始的IEC代码添加至PLCGenerator生成的代码 编辑步骤名称 编辑跃迁 编辑工具栏 编辑选择 斜坡经过时间 元素： 空的 不允许空维度。 启用 输入一个名字 输入一个端口号 输入界面的ip用以绑定 列举的 等于 错误 错误：在PLC中，必须申明至少一个配置和一个资源！
 错误：IEC到C编译器返回 %d
 错误在ST/IL/SFC代码生成器中：
%s
 存储时有错误 "%s"
 错误：导出从站失败
 错误: Modbus/IP 服务器 %{a1}.x 和 %{a2}.x 使用相同的端口号 {a3}.
 错误：没有主控生成
 错误：没有PLC构建
 连接时存在异常 %s!
 执行控制： 执行命令： 实验性的WEB基础的HMI 指数 幂 导出 CANOpen 从站到 EDS 文件 导出图形值到剪切板 导出从站 表达式： 外部的 正在提取位置变量......
 微分项的FB 积分项的FB 功能块图 失败：传输之前必须构建。
 下降沿 致命错误：无法获取构建者。
 抓取 %s 领域 %s 没有一个有效值！ 领域 %s 没有一个有效值！ 文件 '%s' 已经存在！ 查找 查找下一个 查找前一个 查找位置 查找： 强制值 强制变量值 形式不完整。%s 必须被填补完整！ 形式不完整。%s 名字必须填！ 形式不完整。%s 有效的块类型必须被选择！ 向前的 功能 功能 &块 功能块 功能块类型 功能块 功能块不能用于功能中！ 功能块 "%s" 不能用于功能中！ 功能 生成程序 生成软PLC IEC-61131 ST/IL/SFC 代码......
 一般的 全球的 定位当前值 图形 大于 大于或等于 栅格分辨率： HTTP 界面端口： 高度： 主目录 水平： 小时： IEC_频道 指令表编程语言 IP IP 无效！ 导入 SVG 输入输出 InOut 变量 {a1} 在块 {a2} 在 POU{a3} 必须被连接. 不活动  "{a1}" 和 "{a2}" 数据类型不相容  "%s" 和  "BOOL" 数据尺寸不相容  "{a1}" 和 "{a2}" 数据尺寸不相容 指示器 初始的 初始的步 初始值 初始值： 初始值 Inkscape 在线 输入 微分输入 输入变量 输入： 插入 有id的实例 %d 尚不存在！ 实例： 积分输出 界面 中断 区间 无效的URL!
请输入正确的URL地址. 无效的plcopen元素！！！ 无效类型 "{a1}"-> {a2} != {a3} 对于定位 "{a4}" 无效类型 "{a1}"-> {a2} != {a3} 对于定位 "{a4}" 无效值 "%s" 为调试变量 无效值 "%s" 对于变量网格元素 无效值 "%s" 在视窗块 无效值 "{a1}" 对于 "{a2}" 变量！ 无效值！
你必须填入一个数字值。 调用率ms单位 是安全连接吗？ 跳转 梯级图 LDFLAGS 有id的梯形元素 %d  不止在一个梯级上。 语言 语言（选填）： 语言： 最新构建已经与当前目标匹配。正在传输中......
 启动 WX GUI 检查员 启动一个活的Python Shell 左 左电源导轨 字符串长度 小于 小于或等于 库 库 许可 限制 链接  链接：
 Linux 本地 本地入口 本地服务探索失败！
 本地IP地址 本地端口号 位置 存在的定位： 底数10的对数 MDNS 解析度失败因为 '%s'
 手动输出调节 - 通常来自中转站 映射变量 通过CANopen映射位置变量 通过Modbus定位变量 主站 最大计数 ({a1}) 达到了对于类型 {a2} 这个confnode 最大远程TCP客户端 最大值 最大值： 存储 存储区域 存储区域类型 菜单工具栏 微秒： 中间 毫秒： 最小值 最小值： 分： 其他 Modbus 支持 ModbusRTC客户端 ModbusRTU从站 Modbus请求 Modbus根 Modbus服务器节点 ModubsTCP客户端 改动： 多个连接器发现对应在 "{a2}" POU中 "{a1}" 延续 下移动作 上移动作 下移 下移元素 上移元素 下移实例 上移实例 下移任务 上移任务 移动视图 上移 下移变量 上移变量 多路器（多选一） 乘法 我的计算机： NAME NOT R1 名字 名称不能为空！ 名字： 自然对数 否定 Nevow Web 服务失败。  Nevow/Athena 导入失败： 新建 新建项目 无改动 没有PLC可传输（构建是否成功？）
 在 "%s" POU 中没有任何东西被定义 没有连接器发现对应在 "{a2}" POU中 "{a1}" 延续 没有文件可用。
稍候 对于 "%s" 块没有信息发现 无输出{a1}变量发现在 {a3} POU的 {a2} 块。 连接必须断开 没有存在的搜索结果。 没有这样的SVG文件：%s
 没有如此 标签/子标签 ({a1},{a2}) (变量 {a3}) 没有如此 标签/子标签 ({a1},{a2}) 在ID : {a3} (变量 {a4}) 没有有效的值被选择！ 无变量被定义在 "%s" POU 节点Id 不存在节点 ID ：{a1} (variable {a2}) 非保持 正常 不是PDO可映射变量：'{a1}' (ID:{a2},Idx:{a3},sIdx:{a4})) 不等于 通道号 序列号： 数学式 在改变中 唯一元素 打开 打开 Inkscape 开源自动化框架，实现不断增长的扩展集和弹性的PLC运行时的IEC 61131-3 IDE 打开一个文件浏览器来管理项目文件 打开 wxGlade 选项 选项 组织（选填）： 其他配置 输出 压倒一切的复位 PDO 接收 PDO 传输 PLC：
 PLC 记录 PLC 代码生成失败！
 PLC 是空的或者已经被启动。 PLC 没有被启动。 PLC语法错误在行 {a1}:
{a2} PLCOpen 文件 (*.xml)|*.xml|所有文件|*.* PLCOpen编辑器 PLCOpenEditor是Beremiz项目的一部分。

Beremiz是一个 端口 POU 名字 POU 名字： POU类型 POU 类型： PV - SP PYRO 连接到 URI ： %s
 PYRO使用认证在 '%s' 
 页面设置 页面大小（选填）： 页：%d 父实例 奇偶校验 粘贴 粘贴POU 检索模式： 插脚数： 请选择一个目标 请输入一个块名称 请输入注释文本 请输入步骤名称 请输入文本 请输入值对于一个 "%s" 变量： 端口号必须为 0 <= 端口号 <= 65535！ 端口号必须是整数！ 电源导轨 电源导轨属性 当前日期 打印预览 预览： 打印 打印预览 优先 优先： 故障启动PLC：错误 %d 过程变量 产品名称 产品名字（必填）： 产品发布（选填）： 产品版本 产品版本（必填）： 程序 该编程成功生成文件！ 程序 程序不能被其它POU使用！ 项目 项目 '%s'： 项目文件 项目名称 项目名称（必填）： 项目版本（选填）： 项目文件语法错误：

 项目属性 项目树的布局不匹配confnode.xml {a1}!={a2}  扩展名 属性 比例常数 在本地网络上发布服务 Pyro异常： %s
 Pyro端口： Python代码 Python文件 合格验证 退出 斜坡时间 范围： 原始的IEC代码 真的删除节点 '%s'吗？ 范围: 重做 参照 刷新 正则表达式 正则表达式 释放值 余数（模） 远程IP地址 远程端口号 移除 %s 节点 移除数据类型 移除POU 移除动作 移除元素 从左侧目录移除文件 移除实例 移除从站 移除任务 移除变量 重命名 替换文件 通过连接替换Wire 替换 重置 重置执行命令 复位透视图 复位搜索结果 复位时间 资源 保持 返回类型： 右 右电源导轨 上升沿 循环左移 循环右移 四舍五入 运行 运行时IO扩展C代码生成失败！
 运行时库扩展C代码生成失败！
 SDO客户端 SDO服务器 顺序功能图 POU "{a1}" 中的SFC跳转 涉及不存在SFC步 "{a2}" 在POU "%s" 中 SFC 移动必须被连接。 结构化文本 ST 文件 (*.st)|*.st|所有文件|*.* SVG 文件 (*.svg)|*.svg|All files|*.* SVGUI 采样周期 保存 另存为... 另存为 保存路径和项目路径相同！
 范围 搜索 在项目中搜索 秒： 选择全部 选择一个变量种类： 选择一个编辑： 选择一个实例 选择一个对象 选择的目录已经包含其他项目。覆盖？
 选择 选择收敛 选择发散 串行端口 服务探索 存在的服务： 设置 设置点 左移 右移 显示由PLCGenerator生成的IEC代码 显示主控 显示由config_utils生成的主控 显示代码 同步收敛 同步发散 正弦 单 从站ID 源代码没有变化，不需要构建.
 源信号必须被定义对于一个单独任务 '{a1}' 在资源 '{a2}.{a3}'. 平方根（底数2） 标准功能类型 开始PLC 开始建立 %s
 起始地址 已开始 启动PLC
 状态工具栏 步 停止 停止PLC 停止运行PLC 停止位 已停止 结构的 子集的 减法 成功构建.
 切换视图 同步_对齐 同步_对齐_比率 Sync_TPDOs 在模式搜索正则表达式语法错误！ 类型 正切 目标类型 任务  任务： 缓冲 PID（比例，积分，微分）功能块提供经典的三项控制器，实现闭环控制。 （RAMP）斜坡功能块是根据标准中给出的示例建模的。 RS双稳态是一个复位优先锁存器 SR双稳态是一个置位有限锁存器 关于Beremiz/PLCOpenEditor的最佳问问题点
是项目的邮件列表： beremiz-devel@lists.sourceforge.net

这主要是社区支持渠道。
发布必须订阅邮件列表。

你可在这里订阅列表：
https://lists.sourceforge.net/lists/listinfo/beremiz-devel 微分功能块处理一个输出XOUT 比例于输入XIN的变化率. 当计数值从一个预设值减到0，减计数器能被用来产生信号。 文件 '%s' 已经存在。
你真的要替换它？ 块的组必须是连贯的！ 迟滞功能块提供一个两个差异的浮点（实数）输入XIN1和XIN2驱动的迟滞布尔输出。 积分功能块集成了一段时间输入XIN的值 断开延迟定时器能被用来延迟设置一个输出为FALSE，当输入到FALSE后经过一个固定的时间。 接通延迟定时器能被用来延迟设置一个输出为TRUE，当输入为TRUE时，经过固定的时间， 当检测到下降沿时，输出产生一个单独的脉冲。 当检测到一个上升沿时，输出产生一个单独的脉冲。 脉冲定时器能够被用来产生给定持续时间的输出脉冲 实时时钟大量用于包含时间戳，设置在批处理报告，报警信息等中的日期和日中的时间， 信号量提供一种机制，允许软件元素互斥排他的访问某一资源。 当计数值达到最大值时，加计数器能被用来产生信号。 加减计数器有2个输入CU和CD，能被用来同时在一个输入上加计数在另一个上减计数。 文件已被改动。你希望保存吗？ 一个编程组织单元被命名为"%s"。这可能会产生冲突。你希望继续吗？ 打印出现问题。
请检查你当前打印机设置。 该选项尚未可用！ 滴答： %d 时间 时间加法 时间级联 时间除法 时间乘法 时间减法 日期时间加法 日期时间减法 超时 ms单位 触发值 顶部 传输 传输PLC 传输成功
 传输失败
 转换 转换 "%s" 体必须包含一个输出变量或圈指的是它的名字 转换名字 转换名字： 转换的内容 "{a1}" 没有连接到下一步在 "{a2}" POU 转换的内容 "{a1}" 没有连接到前一步在 "{a2}" POU 已命名的转换 %s 尚不存在！ 转换 转换因为 触发 扭曲的不可用。 类型 类型和派生 位置的冲突类型 "%s" 类型转换 类型信息： 严格类型 类型： URI主机: URI端口: URI 类型: URI_位置 对于 %02x节点不能定义POU映射 不能获得 Xenomai 的 %s 
 未定义块类型 "{a1}" 在 "{a2}" POU中 未定义的pou类型 撤销 未知 未知的变量 "%s" 这个POU！ 未命名 未命名%d 无法识别数据大小 "%s" 用户数据类型 用户类型 用户 - 定义POUs 值 值： 变量 变量Drop 变量属性 变量种类 变量不属于这个POU！ 变量： 变量 垂直的： WAMP 客户端连接失败 (%s) .. 重试 .. WAMP 客户端连接丢失 (%s) .. 重试 .. WAMP ID: WAMP 客户端连接到 : WAMP 客户端连接没有建立! WAMP客户端启动失败。 WAMP 配置不完整. WAMP 配置丢失. WAMP 连接到 URL : %s
 WAMP连接超时 WAMP 连接到 '%s' 失败。
 WAMP导入失败： WAMP 加载错误:  WAMP 会话残留 WXGLADE 用户图形界面 Wamp 加密加载错误: 警告 警告在ST/IL/SFC代码生成器中：
 整个项目 宽度： Win32 包搜索 出自 WxGlade GUI XenoConfig Xenomai 你没有写入的许可。
无论如何都打开Inkscape？ 你没有写入的许可。
无论如何都打开wxGlade？ 你必须有在此项目上工作的许可
在项目的一个拷贝上工作？ 你必须选择一个块或块的组围绕着需被添加的分支！ 你必须选择一条线连接需被添加的接触点！ 你必须输入一个名字！ 你必须输入一个值！ 显示比例 类 日 降序（desc） 错误：%s
 退出伴随状态 {a1} (pid {a2})
 第一个输入参数 第一个输出参数 功能 功能块 小时 初始的 内部状态：0-复位， 1-计数，2-设置 matiec安装没有发现
 毫秒 分 名字 事件（onchange） 挑选（opts） 程序 第二个输入参数 第二个输出参数 秒 从中间取字符串 从左取字符串 从右取字符串 类型 更新信息不存在 变量 变量 警告：%s
 {a1} "{a2}" 不能被粘贴作为一个 {a3}. {a1} XML文件没有遵循XSD schema在行 {a2}:
{a3} 