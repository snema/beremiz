��    X     �  )  �%      @2  �   A2     3     3     3      3  	   )3     33     ;3     C3      Y3     z3     �3     �3     �3  %   �3  $   
4     /4  )   N4  +   x4  $   �4     �4     �4     �4  :   5     Z5     s5     �5  )   �5  V   �5  ;   *6  !   f6  :   �6  9   �6     �6     7     7     7     7     7     7     '7  
   /7     :7     X7     q7     ~7     �7  
   �7     �7     �7     �7     �7  	   �7     �7     �7     �7     �7     �7  	   �7     8      $8     E8     `8     m8     r8     �8     �8     �8     �8  V   �8      �8     9  )   .9  8   X9  ,   �9     �9     �9     �9     �9     �9     �9  "   :     $:     ,:     5:     <:  
   @:     K:     ]:     d:     l:     y:     �:     �:     �:     �:     �:  
   �:     �:     ;  	   ;     ;     %;     2;     ;;     V;  	   h;     r;  +   v;  %   �;  7   �;  .    <  
   /<     :<     C<  
   O<  6   Z<     �<     �<     �<     �<     �<  
   �<     �<     �<     �<     =     )=     F=     R=     b=     p=  5   �=  "   �=  +   �=  2   >     8>     D>     W>     p>  "   �>     �>     �>     �>     �>  )   �>  B   !?  >   d?     �?     �?     �?     �?     �?     �?     �?  	   @     @     @     @     .@     ;@     T@  
   l@     w@  %   �@     �@     �@     �@     �@  
   �@     A     A     -A     GA     YA  	   uA     A     �A     �A     �A     �A     �A     �A     �A     B     B     !B     *B     1B     GB  
   \B     gB     xB     �B     �B     �B     �B     �B     �B     C     %C     ;C     CC     OC     \C  	   hC  
   rC     }C  
   �C     �C     �C     �C  +   �C     �C     �C     �C  
   D     D  	   D  	   %D     /D     ?D  
   PD     [D     aD     �D     �D  %   �D  
   �D     �D     �D  &   �D  '   
E     2E     KE     gE     �E     �E     �E     �E     �E      �E     F     F     +F      4F     UF     YF     fF      �F     �F     �F  	   �F     �F     �F     �F     �F     �F  '   G  )   <G  7   fG     �G     �G  /   �G     �G     �G     H     H     )H     1H     =H     DH     GH     JH  
   [H     fH  1   oH  1   �H  3   �H  	   I     I     I     'I     6I     ?I     EI     MI  "   ^I  
   �I  	   �I  	   �I     �I  .   �I     �I  7   �I  6   .J  %   eJ  )   �J  -   �J     �J     �J     �J     K     
K  	   K  C   )K     mK     �K     �K  	   �K     �K  	   �K     �K     �K  
   �K  
   �K     �K     �K      �K     L     !L     6L     KL  "   XL  !   {L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     M     M  	   #M  P   -M     ~M     �M  	   �M     �M     �M     �M     �M     �M     �M     N     N     (N     DN     SN     `N     eN     jN     �N     �N     �N     �N     �N     �N     �N     �N  )   �N  (   O  $   EO  R   jO     �O     �O     �O     	P  +   )P  
   UP     `P     gP     tP  	   �P     �P     �P  �   �P  ,   *Q     WQ     dQ     kQ     sQ     �Q     �Q     �Q     �Q     �Q     �Q     �Q      �Q     R  #   R  )   @R     jR  9   xR     �R     �R  	   �R     �R  	   �R     �R  !   �R  
   S     'S     =S     FS     MS  	   SS     ]S     pS     �S     �S     �S  '   �S  '   �S     T     =T     ET     NT     TT     bT  	   kT     uT     �T     �T     �T     �T     �T     U  #   U     /U  %   8U     ^U     fU     tU     �U     �U     �U     �U     �U  9   �U  
   .V  #   9V     ]V     qV     }V     �V     �V     �V     �V     �V  	   �V     �V     �V     �V     �V     W  
   W     %W     3W     BW     RW     _W     kW     {W     �W     �W     �W     �W  	   �W     �W     �W     �W     �W     �W  
    X  
   X     X     X     X     #X  
   (X     3X     ;X     BX     TX  
   ]X     hX     �X     �X     �X  A   �X  	   �X     Y     Y  
   )Y     4Y  '   @Y     hY  	   tY     ~Y     �Y     �Y  	   �Y     �Y     �Y     �Y     �Y     �Y     �Y     Z     Z  	   Z     $Z     0Z     EZ     XZ     `Z     eZ     lZ  H  qZ  7   �[  $   �[  '   \  P   ?\  P   �\      �\     ]     ]     ]     (]     6]     J]     []     p]     �]     �]     �]  !   �]     �]  
   �]     �]     �]     �]  #   ^     (^  	   0^     :^     V^     h^     n^     v^     ^     �^  "   �^  	   �^  	   �^  1   �^  /   _     ;_     D_  '   `_     �_     �_     �_     �_     �_      `     .`     C`     U`     \`  
   b`     m`  8   y`  7   �`  H   �`     3a     Ia     `a     ea     ka  
   pa     {a     �a  !   �a     �a     �a     �a     �a     �a     �a     �a  	   �a  �  �a  �   rc     Ad     Id     Od     Ud     [d     bd     gd     ld     �d     �d     �d     �d     �d  "   e  #   4e     Xe  '   se  0   �e  :   �e     f     #f  #   Cf  C   gf     �f     �f     �f  8   �f  s   )g  B   �g     �g  6    h  7   7h     oh     vh     zh     ~h     �h     �h     �h     �h  	   �h     �h     �h     �h     �h     �h  
   	i     i     i     %i     -i  	   6i     @i  	   Gi  	   Qi     [i     ii  	   |i  )   �i  *   �i      �i     �i     j     j     4j     7j     :j     =j  X   @j     �j  '   �j  '   �j  1   k  )   8k     bk     hk     yk     �k     �k     �k  #   �k     �k     �k     �k  	   �k     �k     l     l     +l     9l     Kl     al     {l     �l     �l  ,   �l     �l     	m     m     1m     Cm     Tm     hm     qm     �m     �m     �m  7   �m  $   �m  4   n  +   Ln     xn  	   �n     �n     �n  <   �n     �n     �n     o     o     o     *o     8o  	   So  !   ]o     o  "   �o     �o     �o     �o     �o  J   �o  6   Dp  2   {p  W   �p  
   q     q     (q     Eq  +   \q     �q     �q     �q  !   �q  /   �q  L   r  V   ir     �r     �r     �r     �r     s     s     !s  
   0s     ;s     Bs     Ns     ds  !   vs     �s     �s  
   �s  (   �s     �s  &   t     ,t     5t     Jt     St     lt     �t     �t     �t     �t     �t  	   �t     �t  $   �t     u     'u     9u     Mu     bu     ~u  
   �u     �u  "   �u      �u  	   �u     �u     �u     v     ,v     @v     Vv     pv     �v     �v     �v  	   �v     �v     �v     �v  	   w  
   w     w     "w     .w     @w     Mw  (   Vw     w     �w  	   �w     �w     �w     �w     �w     �w     �w  
   �w     
x  "   x     3x     Bx  &   ]x  	   �x     �x     �x  (   �x  *   �x     �x  (   y     .y  $   Jy     oy     �y     �y     �y  )   �y     �y     z     z  !   z     =z     Az      Rz  %   sz     �z  	   �z     �z     �z     �z  
   �z      {     {  :   +{  >   f{  Q   �{     �{      |  2   |     B|     I|  	   \|     f|     {|     �|     �|     �|     �|     �|     �|     �|  3   �|  /   �|  3   #}  	   W}     a}     i}     w}     �}     �}  	   �}     �}  !   �}     �}  	   �}     �}  	   �}  <    ~  $   =~  ?   b~  >   �~  2   �~  ,     7   A     y     �     �     �     �     �  Q   �     �     %�     .�  	   F�     P�     e�  
   q�     |�     ��  
   ��     ��     ��  '   ��     Ѐ     ހ     ��     �  &   #�  %   J�     p�     w�     �     ��     ��     ��     ��     ��     ρ     ׁ     ��     �     ��     �  R   �     d�     |�     ��     ��     ��     ׂ     �     �     %�     5�     P�      j�     ��     ��     ��     ��     ��     ΃     ԃ     �     �  #   
�     .�  	   3�     =�  ;   M�  )   ��  2   ��  g   �  %   N�     t�  !   ��  &   ��  ,   Յ  
   �     �     �     !�  	   9�     C�     I�  �   X�  B   �     4�     B�     J�     S�     m�     z�     ��     ��     ��     ��  &   ��  "   Շ     ��  *   �  4   >�     s�  <   ��     ��     Ĉ     Ј     ݈     �     ��  &   �     :�     M�     l�     x�     ��  	   ��     ��     ��  %   É  %   �     �  0   (�  .   Y�  %   ��     ��     ��     ӊ     ܊  
   �     ��  (   	�     2�     B�  "   b�     ��  "   ��     ��  "   ċ  	   �  0   �     "�     *�     8�     L�     \�     |�  (   ��     Č  I   ܌     &�  !   3�     U�     m�     |�     ��     ��     ��  $   ��     ͍     Ս  	   �     �     ��     �     $�     3�     ?�     N�     _�     r�     ��     ��     ��     ��     ��  	   ܎     �     �     �     �     %�     ;�     P�     V�     b�     o�     s�     v�     |�     ��     ��     ��     ��  	   ��     ��  $   я     ��     �     &�  =   ;�  	   y�     ��     ��     ��     ̐  ,   �     �      �     0�     5�     L�     g�     s�     ��     ��     ��     ��  	   ��     Ǒ     ߑ  	   �     �     ��     �     *�     3�     :�     C�  _  O�  0   ��  #   ��      �  K   %�  M   q�  *   ��     �     �     �     �     ,�     E�     Z�     r�     ��  
   ��     ��  '   ��     ޕ     ��     �     �     �  *   (�     S�     \�  %   h�     ��     ��     ��  	   ��     Ė     ޖ  #   �  
   �  
   !�  B   ,�  @   o�     ��     ��  ,   ԗ  )   �     +�  #   K�     o�     ��  "   ��     Ƙ     �     ��     �     �     �  ;   %�  @   a�  V   ��     ��     �     2�     7�     >�  	   C�     M�     S�  '   Z�     ��     ��     ��     ��     ��     ��  	   ��  
   ��     �  .  �   �         :  �      �           L      a  �  ~   �    0  D  �   �      A  �   /  ?  |      �       �  �      #        '  �   Q  p     Y            �    �   h      �  �        �   �              _  �      s     �  5  �   a     >      C   d  9   �  D       �         E   v  �      +  h   F   �   1  �  j       �     l   �    �  �  �  �  u  0  �   *      �      �                 L          `          �   �               i   j  W  �   4  <  :   ;      �   !   6  /      c       *   k      J  2  �  '      @  �     �   {  �       J   I  �  1   �      8  q          �           F      �  ?        p    $  �   �      5   ,             �   P  �      �   �  �  �      �   �             3  Q      �  9  �           o   �  �  �   ,   �   �   �  ?   '   �   �        �   �   @   �         |       .  �          =         �  �         
       �         �  Y  r       �  �  N              M   (  K  x   =      �  �  �       H    �   �   �   �                �       �   g  �   �               o                      O  -   �   +  #  �      B      �  X         n  �   �   �       �     �  .       �  �   �      U   !  �   �  �   �  D  B   �  �   U  �    *  �  R  K   �      �          E                      V  +   �  �     �             $  7  �          �  ^   �  �   �   �  �  O  �  �      1      �  �   �   �  �   �   
  X  �   �   B          �  F  `   y  �       6     �                  �   T      �   �  )   �   �   �      �   H   �    q       ,      �         ]       i  �       �  �         �   �   �  4   G  <  �  �   �     �  �       }  w      L   b  �       �  y   \  �   �  w                     �   �                �   d   /  �      �  �  R   z   f   �  &  �  �   8       �    N   �      ;               }   !     �      R  �          :  �  ^      �   W          �  A  �  �  �  �  #          �           �  �  S      �       �                   �        �   W   \   U  �  "  �   ]      �   r        �   P  �       @         �   [  0   C  �   z  �  �       V  c      3      �   �           �            %      	   O   2       S   �   e       �          b       �  �   �  M  X   -  �  �   �  -      _         �  �   g           >  �   �   �                 A     �  M  2      k   4  H  �     f  =   u   �  �   �  9      )    6   v     N  �     �       $   �     �   %   �      �  &  &   7       P   �   l  (   x  �       �  �   �  Z   m         �  �    [         �         V       ;           	  5      �   �   K  I     ~      �     <   �       G      7  s  T  )          �           e      %          �      (      n   �      T   �  �  G   �       t       �                  �   �   �  
      "      �   {   t  �       Q   E  �  3      �   �  >   J  �         �              �      8      C  �  I          �   S      Z  �             �                	              �   "               m   
An unhandled exception (bug) occured. Bug report saved at :
(%s)

Please be kind enough to send this file to:
beremiz-devel@lists.sourceforge.net

You should now restart program.

Traceback:
    External    InOut    Input    Local    Output    Temp  and %s  generation failed !
 "%s" Data Type doesn't exist !!! "%s" POU already exists !!! "%s" POU doesn't exist !!! "%s" can't use itself! "%s" config already exists! "%s" configuration already exists !!! "%s" configuration doesn't exist !!! "%s" data type already exists! "%s" element for this pou already exists! "%s" folder is not a valid Beremiz project
 "%s" is a keyword. It can't be used! "%s" is an invalid value! "%s" is not a valid folder! "%s" is not a valid identifier! "%s" is used by one or more POUs. Do you wish to continue? "%s" pou already exists! "%s" step already exists! "%s" value already defined! "%s" value isn't a valid array dimension! "%s" value isn't a valid array dimension!
Right value must be greater than left value. "{a1}" function cancelled in "{a2}" POU: No input connected "{a1}" is already used by "{a2}"! "{a1}" resource already exists in "{a2}" configuration !!! "{a1}" resource doesn't exist in "{a2}" configuration !!! %03gms %dd %dh %dm %dms %ds %s Data Types %s POUs %s Profile %s body don't have instances! %s body don't have text! &Add Element &Close &Configuration &Data Type &Delete &Display &Edit &File &Function &Help &License &Program &Properties &Recent Projects &Resource '{a1}' - {a2} match in project '{a1}' - {a2} matches in project '{a1}' is located at {a2}
 (%d matches) , %s 0 - manual , 1 - automatic 1d 1h 1m 1s A POU has an element named "%s". This could cause a conflict. Do you wish to continue? A POU named "%s" already exists! A location must be selected! A task with the same name already exists! A variable with "%s" as name already exists in this pou! A variable with "%s" as name already exists! About Absolute number Action Action Block Action Name Action Name: Action with name %s doesn't exist! Actions Actions: Active Add Add Action Add Configuration Add IP Add POU Add Resource Add Transition Add Wire Segment Add a new initial step Add a new jump Add a new step Add a simple WxGlade based GUI. Add action Add element Add instance Add slave Add task Add variable Addition Additional function blocks Adjust Block Size Alignment All All files (*.*)|*.*|CSV files (*.csv)|*.csv Already connected. Please disconnect
 An element named "%s" already exists in this structure! An instance with the same name already exists! Arc cosine Arc sine Arc tangent Arithmetic At least a variable or an expression must be selected! Author Author Name (optional): Beremiz Block Block Properties Block name C Build failed.
 C code C code generated successfully.
 C compilation failed.
 C compilation of %s failed.
 C extension CANOpen network CANOpen slave CANopen support Can only give a location to local or global variables Can't generate program to file %s! Cannot get PLC status - connection failed.
 Cannot transfer while PLC is running. Stop it now? Change Name Change Port Number Change working directory Choose a SVG file Choose a directory to save project Choose a file Choose a project Choose a value for %s: Choose a working directory  Choose an empty directory for new project Chosen folder doesn't contain a program. It's not a valid project! Chosen folder isn't empty. You can't use it for a new project! Clean Clean log messages Clear Errors Clear Execution Order Close Close Application Close Project Close Tab Coil Comment Community support Company Name Company Name (required): Company URL (optional): Comparison Compiler Compiling IEC Program into C code...
 Concatenation Confirm or change variable name Connect Connect to the target PLC Connection Connection Properties Connection canceled!
 Connection failed to %s!
 Connection lost!
 Connection to '%s' failed.
 Connector Connectors: Constant Contact Content Description (optional): Continuation Conversion from BCD Conversion to BCD Conversion to date Conversion to time-of-day Copy Copy POU Cosine Couldn't start PLC !
 Couldn't stop PLC !
 Create HMI Create a new POU Create a new action Create a new action block Create a new block Create a new coil Create a new comment Create a new connection Create a new contact Create a new transition Create a new variable Credits Description Description: Dimensions: Direction Direction: Directly Disconnect Disconnect from PLC Disconnected Division Do you really want to delete the file '%s'? Documentation Done Duration Edit Block Edit comment Edit file Edit item Edit transition Editor selection Elements : Empty Empty dimension isn't allowed. Enter a name  Enter a port number  Enter the IP of the interface to bind Enumerated Equal to Error Error : IEC to C compiler returned %d
 Error in ST/IL/SFC code generator :
%s
 Error while saving "%s"
 Error: Export slave failed
 Error: No Master generated
 Exception while connecting %s!
 Execution Control: Execution Order: Experimental web based HMI Exponentiation Export CanOpen slave to EDS file Export slave Expression: External Extracting Located Variables...
 FBD Falling Edge Field %s hasn't a valid value! Fields %s haven't a valid value! File '%s' already exists! Find Find Next Find Previous Find position Find: Force value Forcing Variable Value Form isn't complete. %s must be filled! Form isn't complete. Name must be filled! Form isn't complete. Valid block type must be selected! Function Generate Program Generating SoftPLC IEC-61131 ST/IL/SFC code...
 Global Go to current value Greater than Greater than or equal to Height: Horizontal: Hours: IL IP IP is not valid! Import SVG Inactive Incompatible data types between "{a1}" and "{a2}" Incompatible size of data between "%s" and "BOOL" Incompatible size of data between "{a1}" and "{a2}" Indicator Initial Initial Value Initial Value: Inkscape Input Inputs: Insertion (into) Instance with id %d doesn't exist! Instances: Interface Interrupt Interval Invalid URL!
Please enter correct URL address. Invalid plcopen element(s)!!! Invalid type "{a1}"-> {a2} != {a3}  for location "{a4}" Invalid type "{a1}"-> {a2} != {a3} for location "{a4}" Invalid value "%s" for debug variable Invalid value "{a1}" for "{a2}" variable! Invalid value!
You must fill a numeric value. Is connection secure? Jump LD Language Language (optional): Language: Latest build already matches current target. Transfering anyway...
 Launch WX GUI inspector Left Length of string Less than Less than or equal to Libraries Library License Limitation Linking :
 Linux Local Local service discovery failed!
 Location Locations available: Logarithm to base 10 Map Variable Map located variables over CANopen Map located variables over Modbus Master Maximum Maximum: Memory Menu ToolBar Microseconds: Middle Milliseconds: Minimum Minimum: Minutes: Miscellaneous Modbus support Modifier: More than one connector found corresponding to "{a1}" continuation in "{a2}" POU Move action down Move action up Move down Move element down Move element up Move instance down Move instance up Move the view Move up Move variable down Move variable up Multiplexer (select 1 of N) Multiplication My Computer: NAME Name Name must not be null! Name: Natural logarithm Negated Nevow Web service failed.  Nevow/Athena import failed : New New item No Modifier No PLC to transfer (did build succeed ?)
 No documentation available.
Coming soon. No informations found for "%s" block No output {a1} variable found in block {a2} in POU {a3}. Connection must be broken No search results available. No such SVG file: %s
 No valid value selected! No variable defined in "%s" POU Non existing node ID : {a1} (variable {a2}) Non-Retain Normal Not equal to Number of sequences: Numerical Open Open Inkscape Open Source framework for automation, implemented IEC 61131 IDE with constantly growing set of extensions and flexible PLC runtime. Open a file explorer to manage project files Open wxGlade Option Options Organization (optional): Other Profile Output PDO Receive PDO Transmit PLC :
 PLC Log PLC code generation failed !
 PLC is empty or already started. PLC is not started. PLC syntax error at line {a1}:
{a2} PLCOpen files (*.xml)|*.xml|All files|*.* PLCOpenEditor PLCOpenEditor is part of Beremiz project.

Beremiz is an  PORT POU Name POU Name: POU Type POU Type: PYRO connecting to URI : %s
 PYRO using certificates in '%s' 
 Page Setup Page Size (optional): Page: %d Parity Paste Paste POU Pattern to search: Please choose a target Please enter a block name Please enter comment text Please enter text Please enter value for a "%s" variable: Port number must be 0 <= port <= 65535! Port number must be an integer! Preview Preview: Print Print preview Priority Priority: Problem starting PLC : error %d Product Name Product Name (required): Product Release (optional): Product Version Product Version (required): Program Program was successfully generated! Programs Programs can't be used by other POUs! Project Project '%s': Project Files Project Name Project Name (required): Project Version (optional): Project file syntax error:

 Project properties Project tree layout do not match confnode.xml {a1}!={a2}  Properties Publishing service on local network Pyro exception: %s
 Pyro port : Python code Python file Quit Range: Really delete node '%s'? Redo Reference Refresh Regular expression Regular expressions Remainder (modulo) Remove %s node Remove Pou Remove action Remove element Remove instance Remove slave Remove task Remove variable Rename Replace File Replace Wire by connections Reset Reset Execution Order Resources Right Rising Edge Rotate left Rotate right Run SDO Client SDO Server SFC ST SVGUI Save Save As... Save as Search Search in Project Seconds: Select All Select a variable class: Select an editor: Select an instance Select an object Selected directory already contains another project. Overwrite? 
 Selection Service Discovery Services available: Shift left Shift right Show IEC code generated by PLCGenerator Show Master Show code Sine Square root (base 2) Standard function blocks Start PLC Start build in %s
 Started Starting PLC
 Status ToolBar Stop Stop PLC Stop Running PLC Stopped Structure Subtraction Successfully built.
 Switch perspective Tangent Task Tasks: Temp The best place to ask questions about Beremiz/PLCOpenEditor
is project's mailing list: beremiz-devel@lists.sourceforge.net

This is the main community support channel.
For posting it is required to be subscribed to the mailing list.

You can subscribe to the list here:
https://lists.sourceforge.net/lists/listinfo/beremiz-devel The file '%s' already exist.
Do you want to replace it? The group of block must be coherent! There are changes, do you want to save? There is a POU named "%s". This could cause a conflict. Do you wish to continue? There was a problem printing.
Perhaps your current printer is not set correctly? This option isn't available yet! Time Time addition Time concatenation Time division Time multiplication Time subtraction Time-of-day addition Time-of-day subtraction Toggle value Transfer Transfer PLC Transfer completed successfully.
 Transfer failed
 Transition Translated by Undo Unknown Unknown variable "%s" for this POU! Unnamed Unnamed%d Unrecognized data size "%s" User-defined POUs Value Values: Variable Variable Properties Variable class Variable don't belong to this POU! Variable: Variables WAMP Client connection failed (%s) .. retrying .. WAMP Client connection lost (%s) .. retrying .. WAMP ID: WAMP client connecting to : WAMP client connection not established! WAMP client startup failed.  WAMP config is incomplete. WAMP config is missing. WAMP connecting to URL : %s
 WAMP connection timeout WAMP connection to '%s' failed.
 WAMP import failed : WAMP load error:  Width: Win32 Written by WxGlade GUI You don't have write permissions.
Open Inkscape anyway ? You don't have write permissions.
Open wxGlade anyway ? You must have permission to work on the project
Work on a project copy ? You must type a name! You must type a value! Zoom class days error: %s
 hours initial matiec installation is not found
 milliseconds minutes name program seconds type variable variables Project-Id-Version: Beremiz
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Rodrigo Rolle <ro_pita@hotmail.com>, 2018
Language-Team: Portuguese (Brazil) (https://www.transifex.com/beremiz/teams/75746/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 
Ocorreu uma exceção não tratada (bug). Relatório de erro salvo em:
(%s)

Favor enviar este relatório de erro para:
beremiz-devel@lists.sourceforge.net

Você deve reiniciar este programa.

Traceback:
 Externo InOut Input Local Output Temp e %s falha na geração !
 Data Type "%s" não existe !!! POU "%s" já existe !!! POU "%s" não existe !!! "%s" não pode usar a si mesmo! configuração "%s" já existe Configuração "%s" já existe !!! Configuração "%s" não existe !!! Data Type "%s" já existe! Elemento "%s" para este POU já existe! A pasta "%s" não é um projeto Beremiz válido
 "%s" é uma palavra-chave (keyword) e não pode ser usado! "%s" é um valor inválido! "%s" não é uma pasta válida! "%s" não é um identifier válido! "%s" está sendo usado por um ou mais POUs. Você deseja continuar? POU "%s" já existe! Passo "%s" já existe! Valor "%s" já definido! "%s" não é um valor válido para dimensão de vetores! "%s" não é um valor válido para dimensão de vetores!
O valor correto deve ser maior do que o valor da esquerda. Função "{a1}" cancelada em POU "{a2}": Nenhuma entrada conectada "{a1}" já é usado por "{a2}"! Recurso "{a1}" já existe em configuração "{a2}" !!! Recurso "{a1}" não existe em configuração "{a2}" !!! %03gms %dd %dh %dm %dms %ds %s Data Types %s POUs %s Perfil %s não possui instâncias! %s não possui texto! &Adicionar Elemento &Fechar &Configuração &Data Type &Deletar &Exibir &Editar &Arquivo &Função &Ajuda &Licença &Programa &Propriedades &Projetos Recentes &Recursos '{a1}' - {a2} correspondência no projeto '{a1}' - {a2} correspondências no projeto '{a1}' está localizado em {a2}
 (%d correspondências) , %s 0 - manual, 1 - automático 1d 1h 1m 1s Um POU já tem um elemento chamado "%s". Isto pode causar um conflito. Deseja continuar? Já existe POU chamado "%s"! Uma localização deve ser selecionada! Já existe uma tarefa com o mesmo nome! Já existe uma variável com nome "%s" neste POU! Já existe uma variável com o nome "%s"! Sobre Número absoluto Ação Bloco de Ação Nome de Ação Nome de Ação: Não existe ação com o nome "%s"! Ações Ações: Ativo Adicionar Adicionar ação Adicionar Configuração Adicionar IP Adicionar POU Adicionar Recurso Adicionar Transição Adicionar Segmento de Fio Adicionar um novo passo inicial Adicionar um novo salto Adicionar um novo passo Adicionar uma GUI simples baseada em WxGlade Adicionar ação Adicionar elemento Adicionar instância Adicionar escravo Adicionar tarefa Adicionar variável Adição Blocos de função adicionais Ajustar Tamanho do Bloco Alinhamento Todos Todos os arquivos (*.*)|*.*|arquivos CSV  (*.csv)|*.csv Já conectado. Por favor desconecte
 Um elemento chamado "%s" já existe nesta estrutura! Já existe uma instância com o mesmo nome! Arco cosseno Arco seno Arco tangente Aritmética Pelo menos uma variável ou expressão deve ser selecionada! Autor Nome do Autor (opcional): Beremiz Bloco Propriedades do Bloco Nome do Bloco Construção em C falhou.
 Código C Código em C gerado com sucesso.
 Compilação em C falhou!
 Compilação em C de "%s" falhou.
 Extensão em C Rede CANOpen Escravo CANOpen Suporte CANOpen Só é possível atribuir localização para variáveis locais ou globais. Não foi possível gerar programa para o arquivo "%s"! Impossível obter status do CLP - conexão falha.
 Não é possível transferir enquanto o CLP está em execução. Deseja pará-lo agora? Mudar Nome Mudar Número da Porta Mudar diretório de trabalho Escolha um arquivo SVG Escolha um diretório para salvar o projeto Escolha um arquivo Escolha um projeto Escolha um valor para "%s": Escolha um diretório de trabalho Escolha um diretório vazio para o novo projeto O diretório escolhido não contém um programa. Não é um projeto válido! O diretório escolhido não está vazio. Você não pode usá-lo para um novo projeto! Limpar Limpar mensagens do log Limpar Erros Limpar Ordem de Execução Fechar Fechar Aplicação Fechar Projeto Fechar Aba Bobina Comentário Suporte da Comunidade Nome da Companhia Nome da Companhia (obrigatório): URL da Companhia (opcional): Comparação Compilador Compilando programa IEC em código C...
 Concatenação Confirmar ou alterar nome de variável Conectar Conectar ao CLP alvo Conexão Propriedades da Conexão Conexão Cancelada!
 Conexão falha para "%s"!
 Conexão perdida!
 Conexão para '"%s"' falhou.
 Conector Conectores: Constante Contato Descrição do conteúdo (opcional): Continuação Conversão de BCD Conversão para BCD Conversão para data Conversão para hora-do-dia Copiar Copiar POU Cosseno Não foi possível iniciar o CLP!
 Não foi possível parar o CLP!
 Criar IHM Criar um novo POU Criar uma nova ação Criar um novo bloco de ação Criar um novo bloco Criar uma nova bobina Criar um novo comentário Criar uma nova conexão Criar um novo contato Criar uma nova transição Criar uma nova variável Créditos Descrição Descrição: Dimensões: Direção Direção: Diretamente Desconectar Desconecte do CLP Desconectado Divisão Realmente deseja excluir o arquivo '%s'? Documentaçã Feito Duração Editar Bloco Editar comentário Editar arquivo Editar item Editar transição Seleção de editor Elementos: Vazio Dimensão vazia não é permitida. Digite um nome Digite um número de porta Digite o IP da interface para conectar Enumerado Igual a Erro Erro: compilador IEC para C retornou %d
 Erro no gerador de código ST/IL/SFC: 
%s
 Erro ao salvar "%s" 
 Erro: falha na exportação de escravo.
 Erro: nenhum mestre gerado
 Exceção enquanto conectando a %s!
 Controle de Execução: Ordem de Execução: IHM experimental baseada em web Exponenciação Exportar escravo CANopen para arquivo EDS Exportar escravo Expressão: Externo Extraindo Variáveis Alocadas...
 FBD Borda de descida Campo %s não tem valor válido! Campos %s não possuem valor válido! Arquivo '%s' já existe! Localizar Localizar Próximo Localizar Anterior Localizar posiçã Localizar: Forçar valor Forçando valor de variável O formulário não está completo. %s deve ser preenchido! O formulário não está completo. O nome deve ser preenchido! O formulário não está completo. Deve ser selecionado um tipo de bloco válido. Função Gerar Programa Gerando código do SoftPLC IEC-61131 ST/IL/SFC...
 Global Ir para valor atua Maior que Maior que ou igual a Altura: Horizontal: Horas: IL IP IP inválido! Importar SVG Inativo Tipos de dados incompatíveis entre "{a1}" e "{a2}" Tipo de dado incompatível entre "1%s" e "BOOL" Tamanho de dado incompatível entre "{a1}" e "{a2}" Indicador Inicial Valor Inicial Valor Inicial: Inkscape Entrada Entradas: Inserção (em) Instância com ID %d não existe! Instâncias: Interface Interromper Intervalo URL inválido!
Por favor entre com um endereço URL correto. Elemento(s) do PLCOpen inválido(s)! Tipo inválido  "{a1}"-> {a2} != {a3} para localização "{a4}" Tipo inválido "{a1}"-> {a2} != {a3} para localização "{a4}" Valor inválido "%s" para variável de depuração Valor inválido "{a1}" para variável "{a2}" Valor inválido.
Você deve inserir um valor numérico. A conexão é segura? Salto LD Idioma Idioma (opcional): Idioma: Última construção já idêntica com o alvo atual. Transferindo mesmo assim...
 Iniciar Inspetor WX GUI  Esquerdo Comprimento da "string" Menos que Menor que ou igual a Bibliotecas Biblioteca Licença Limitação Linkando:
 Linux Local Falha na descoberta de serviço local!
 Localização Localizações disponíveis: Logaritmo para base 10 Mapear Variável Mapear variáveis alocadas por CANopen Mapear variáveis alocadas por Modbus Mestre Máximo Máximo: Memória Barra de ferramentas Menu Microssegundos: Meio Milissegundos: Mínimo Mínimo: Minutos: Miscelânea Suporte Modbus Modificador: Mais de um conector encontrado correspondente a continuação "{a1}" no POU "{a2}" Mover ação para baixo Mover ação para cima Mover para baixo Mover elemento para baixo Mover elemento para cima Mover instância para baixo Mover instância para cima Mover a visualização Mover para cima Mover variável para baixo Mover variável para cima Multiplexador (selecione 1 de N) Multiplicação Meu Computador: NOME Nome Nome deve ser não-nulo! Nome: Logaritmo natural Negado Falha no serviço Nevow Web. Falha na importação Nevow/Athena: Novo Novo item Sem Modificador Sem CLP para transferir (a construção foi bem-sucedida?)
 Sem documentação disponível.
Em breve. Nenhuma informação encontrada para o bloco "%s"  Nenhuma variável de saída {a1} encontrada no bloco {a2} no POU {a3}. Conexão deve estar interrompida Sem resultados de busca disponíveis. Nenhum arquivo SVG: %s
 Nenhum valor válido selecionado! Nenhuma variável definida no POU "%s" ID de nó inexistente: {a1} (variável {a2}) Não-Reter Normal Não igual a Número de sequências: Numérico Abrir Abrir Inkscape Framework Open Source para automação, IDE implementada IEC 61131 com um conjunto constantemente crescente de extensões e ambiente flexível para CLP. Abrir um explorador de arquivos para gerenciar arquivos de projeto Abrir wxGlade Opção Opções Organização (opcional): Outro Perfil Saída PDO Receber PDO Transmitir CLP:
 Log do CLP: Falha na geração de código do CLP!
 O CLP está vazio ou já iniciado. O CLP não está iniciado. Erro de sintaxe no CLP na linha {a1}:
{a2} Arquivos PLCOpen (*.xml)|*.xml|Todos os arquivos|*.* PLCOpenEditor O PLCOpen Editor é parte do projeto Beremiz.

Beremiz é um PORTA Nome do POU Nome do POU: Tipo do POU Tipo do POU: PYRO conectando à URI : %s
 PYRO utilizando certificados em '%s' 
 Configurar Página Tamanho da Página (opcional): Página: %d Paridade Colar Colar POU Padrão para pesquisar: Por favor escolha um alvo Por favor insira um nome para o bloco Por favor insira texto de comentário Por favor insira o texto Por favor insira um valor para a variável "%s": Número da porta deve ser 0 <= porta <= 65535! Número da porta deve ser um inteiro! Previsualização Previsualização: Imprimir Visualizar impressão Prioridade Prioridade: Problema ao inicializar o CLP: erro "%d" Nome do Produto Nome do Produto (obrigatório): Lançamento do Produto (opcional): Versão do Produto Versão do produto (obrigatório): Programa O programa foi gerado com sucesso! Programas Programas não podem ser usados por outros POUs! Projeto Projeto '%s': Arquivos de Projeto Nome do Projeto Nome do Projeto (obrigatório): Versão do Produto (opcional): Erro de sintaxe no arquivo do projeto:

 Propriedades do projeto Layout da árvore de projeto não compatível com confnode.xml {a1}!={a2} Propriedades Publicando serviço na rede local Exceção do Pyro: %s 
 Porta do Pyro: Código Python Arquivo Python Fechar Faixa: Deseja realmente excluir o nó '%s'? Refazer Referência Atualizar Expressão regular Expressões regulares Resto (módulo) Remover nó %s Remover POU Remover ação Remover elemento Remover instância Remover escravo Remover tarefa Remover variável Renomear Substituir arquivo Substituir Fio por conexões Reiniciar Reiniciar Ordem de Execução Recursos Direita Borda de Subida Girar para a esquerda Girar para a direita Rodar Cliente SDO Servidor SDO SFC ST SVGUI Salvar Salvar Como... Salvar como Buscar Buscar no Projeto Segundos: Selecionar Tudo Selecionar uma classe de variáveis: Selecionar um editor: Selecionar uma instância Selecionar um objeto A pasta selecionada já contém outro projeto. Sobrescrever?
 Seleção Descoberta de Serviços Serviços disponíveis: Deslocar para a esquerda Deslocar para a direita Mostrar código IEC gerado pelo PLCGenerator Mostrar Mestre Mostrar Código Seno Raiz quadrada (base 2) Blocos de função padrão Iniciar CLP Iniciar construção em %s
 Iniciado Iniciando CLP
 Barra de Status Parar Parar PLC Parar Execução do CLP Parado Estrutura Subtração Construído com sucesso.
 Alterar perspectiva Tangente Tarefa Tarefas: Temporário O melhor lugar para enviar perguntas sobre o Beremiz/PLCOpen Editor
é a lista de e=mails do projeto: beremiz-devel@lists.sourceforge.net

Este é o canal de suporte principal da comunidade.
Para enviar é necessário estar inscrito na lista de e-mail.

Você pode se inscrever na lista aqui: https://lists.sourceforge.net/lists/listinfo/beremiz-devel O arquivo '%s' já existe.
Deseja substituí-lo? O grupo do bloco deve ser coerente! Há mudanças, deseja salvá-la? Há um POU com o nome "%s". Isto pode causar um conflito. Deseja continuar? Houve um problema na impressão.
A impressora está configurada corretamente? Esta opção ainda não está disponível! Tempo Adição de tempo Concatenação de tempo Divisão de tempo Multiplicação de tempo Subtração de tempo Adição de hora-do-dia Subtração de hora-do-dia Inverter valor Transferir Transferir CLP Transferência completada com sucesso.
 Falha na transferência.
 Transição Traduzido por Desfazer Desconhecido Variável desconhecida "%s" para este POU! Sem nome Sem nome %d Tamanho de dado não reconhecido "%s" POUs definidos pelo usuário Valor Valores: Variável Propriedades da variável Classe da variável Variável não pertence a este POU! Variável: Variáveis Falha na conexão com cliente WAMP (%s) ... tentando novamente...  Perda de conexão com cliente WAMP (%s)... tentando novamente... ID WAMP: Cliente WAMP conectando a: Conexão com cliente WAMP não estabelecida! Falha na inicialização de cliente WAMP. Configuração WAMP incompleta. Configuração WAMP está faltando. WAMP conectando a URL: %s
 Timeout da conexão WAMP Falha na conexão WAMP para '%s'.
 Falha na importação WAMP : Erro de carregamento WAMP: Largura: Win32 Escrito por GUI WxGlade Você não possui permissão de escrita.
Abrir mesmo assim? Você não tem permissão de escrita.
Abrir WxGlade mesmo assim? É necessária permissão para trabalhar no projeto.
Trabalhar numa cópia do projeto? Você deve digitar um nome! Você deve digitar um valor! Zoom classe dias erro: %s
 horas inicia instalação do matiec não encontrada
 milissegundos minutos nome programa segundos tipo variável variáveis 