#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of Beremiz, a Integrated Development Environment for
# programming IEC 61131-3 automates supporting plcopen standard and CanFestival.
# This file is based on code written for Whyteboard project.
#
# Copyright (c) 2009, 2010 by Steven Sproat
# Copyright (c) 2016 by Andrey Skvortsov <andrej.skvortzov@gmail.com>
#
# See COPYING file for copyrights details.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


"""
This module contains classes extended from wx.Dialog used by the GUI.
"""


from __future__ import absolute_import
import os
import wx
from wx.lib.agw.hyperlink import HyperLinkCtrl
from util import paths as paths
from util.ProcessLogger import ProcessLogger


class UpdaterDialog(wx.Dialog):
    """
    A replacement Updater Dialog for Windows, as it uses a generic frame that
    well...sucks.
    """
    def __init__(self, parent, info):
        title = _("Update ") + " " + info.Name
        wx.Dialog.__init__(self, parent, title=title)
        self.info = info
        self.ide = parent
        if parent and parent.GetIcon():
            self.SetIcon(parent.GetIcon())

        image = None
        if self.info.Icon:
            bitmap = wx.BitmapFromIcon(self.info.Icon)
            image = wx.StaticBitmap(self, bitmap=bitmap)


        check = wx.Button(self, id=wx.ID_ABOUT, label=_("&Check"))
        update = wx.Button(self, label=_("&Update"))
        close = wx.Button(self, id=wx.ID_CANCEL, label=_("&Close"))

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add(check, flag=wx.CENTER | wx.LEFT | wx.RIGHT, border=5)
        btnSizer.Add(update, flag=wx.CENTER | wx.RIGHT, border=5)
        btnSizer.Add(close, flag=wx.CENTER | wx.RIGHT, border=5)

        sizer = wx.BoxSizer(wx.VERTICAL)
        if image:
            sizer.Add(image, flag=wx.CENTER | wx.TOP | wx.BOTTOM, border=5)
        sizer.Add(btnSizer, flag=wx.CENTER | wx.BOTTOM, border=5)

        container = wx.BoxSizer(wx.VERTICAL)
        container.Add(sizer, flag=wx.ALL, border=10)
        self.SetSizer(container)
        self.Layout()
        self.Fit()
        self.Centre()
        self.Show(True)
        self.SetEscapeId(close.GetId())

        check.Bind(wx.EVT_BUTTON, self.on_check)
        update.Bind(wx.EVT_BUTTON, self.on_update)
        close.Bind(wx.EVT_BUTTON, lambda evt: self.Destroy())

    def on_update(self, event):
        UpdateDialog(self, self.info, self.ide)

    def on_check(self, event):
        CheckDialog(self, self.info, self.ide)

beremiz_dir = paths.AbsDir(__file__)

class CheckDialog(wx.Dialog):
    def __init__(self, parent, info, ide):
        wx.Dialog.__init__(self, parent, title=_("Check"), size=(475, 320),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        if parent and parent.GetIcon():
            self.SetIcon(parent.GetIcon())
        self.SetMinSize((300, 200))
        """
        Method called by user to check last version 
        """
        base_dir = beremiz_dir+"/../.."
        ide.Log.write(_("checking version..\n"))
        command = "%s/python3/python-3.7.2/python.exe %s/sofi/generator/generator.py -command check"\
                  % (base_dir, base_dir)
        ProcessLogger(ide.Log, command, no_gui=False,
                      timeout=30.0,).spin


class UpdateDialog(wx.Dialog):
    def __init__(self, parent, info, ide):
        wx.Dialog.__init__(self, parent, title=_("Update"), size=(500, 400),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.SetMinSize((300, 200))
        """
        Method called by user to check last version 
        """
        base_dir = beremiz_dir+"/../.."
        ide.Log.write(_("update version..\n"))
        command = "%s/python3/python-3.7.2/python.exe %s/sofi/generator/generator.py -command update"\
                  % (base_dir, base_dir)
        ProcessLogger(ide.Log, command, no_gui=False,
                      timeout=30.0,).spin


def ShowUpdaterDialog(parent, info):
    if os.name == "nt":
        UpdaterDialog(parent, info)
    else:
        wx.AboutBox(info)
